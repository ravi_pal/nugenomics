<?php /*Template Name: Customer Details*/
   session_start();
   get_header();
   
   if(isset($_POST['qty']))
   {
   $_SESSION['qty']=$_POST['qty'];  
   }
   else
   {
   $_SESSION['qty']=$_SESSION['qty'];  
   }
   if(isset($_POST['productname']))
   {
   $_SESSION['productname']=$_POST['productname']; 
   }
   else
   {
   $_SESSION['productname']=$_SESSION['productname']; 
   }
   
   if(isset($_POST['img_src']))
   {
   $_SESSION['img_src']=$_POST['img_src'];   
   }
   else
   {
   $_SESSION['img_src']=$_SESSION['img_src'];   
   }
//echo $_SESSION['qty'];
   ?>
<?php 

if(isset($_POST['couponSubmit'])){
if(isset($_POST['couponvalue']))
{ //echo $_POST['couponvalue'];
$curr_date = date('Y-m-d');
   $the_querypdc = new WP_Query(array('post_type' => 'coupon', 'meta_key' => 'expirty_date', 's'=>$_POST['couponvalue'], 'orderby'=>'ID', 'order' => 'DESC', 'meta_query' => array(
array(
'key' => 'expirty_date',
'value' => $curr_date,
'type' => 'DATE',
'compare' => '>='
)
)));
   if($the_querypdc->have_posts()): 
   while ($the_querypdc -> have_posts()) : $the_querypdc -> the_post();
$getcouponamt=get_field('coupon_amount');
$_SESSION['couponamt']=$getcouponamt;
$_SESSION['title']=get_the_title();
$_SESSION['sess_msg']='Coupon Successfully Applied!!';
endwhile;
else:
$_SESSION['sess_msg']='Coupon Not Found!';
endif;
wp_reset_query();
//echo $_SESSION['amount'];?>
<script type="text/javascript">
location.href='/customerdetails/';
</script>
<?php
exit();
}
}

if(isset($_POST['resetcoupon']))
{
$_SESSION['couponamt']='';   
$_SESSION['title']='';
$_SESSION['sess_msg']='Coupon Successfully removed!!';
?>
<script type="text/javascript">
location.href='/customerdetails/';
</script>
<?php 
exit();
}

   $the_querypd = new WP_Query(array('post_type' => 'product', 'orderby'=>'ID', 'order' => 'DESC'));
   if($the_querypd->have_posts()): 
   while ($the_querypd -> have_posts()) : $the_querypd -> the_post();
$_SESSION['product_amount']=get_field('product_price')*$_SESSION['qty'];
//echo $_SESSION['couponamt']."<br>";
if($_SESSION['couponamt']!='')
{
//echo "cdscs";
   $_SESSION['product_amount']=$_SESSION['product_amount']-$_SESSION['couponamt'];
//echo $_SESSION['product_amount'];
}
else
{
      $_SESSION['product_amount']=$_SESSION['product_amount'];
}
//echo $_SESSION['product_amount'];
  // $_SESSION['product_amount']=get_field('product_price');
   $_SESSION['shipping']=get_field('shipping_price');
   $_SESSION['gst']=round(get_field('gst_price'));
   $_SESSION['currency']=get_field('currency');
   if($_SESSION['gst']!=''){
   $nppercent=str_replace("%", "", $_SESSION['gst']);
   $finalgst=round(($nppercent*$_SESSION['product_amount'])/100);
   }
   else
   {
   $finalgst='';
   }
   $_SESSION['amount']=$_SESSION['product_amount']+$_SESSION['shipping']+$finalgst;
     ?>
<?php  endwhile; ?>
<?php endif; 
   wp_reset_query();?>
<?php  while ( have_posts() ) : the_post(); 
   $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); 
//echo $_SESSION['amount'];   
   ?>
<link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/css/et_core.css" />
<div id="main-content">
   <div id="et-boc" class="et-boc">
      <div class="et-l et-l--post">
         <div class="et_builder_inner_content et_pb_gutters3">
            <div class="et_pb_section et_pb_with_background et_section_regular" style="background-image: none !important; background-color: #f3f3f1 !important;">
               <div class="et_pb_row et_pb_row_0">
                  <div class="et_pb_column et_pb_column_4_4 et_pb_column_0  et_pb_css_mix_blend_mode_passthrough et-last-child">
                     <div class="et_pb_module et_pb_text et_pb_text_0  et_pb_text_align_center et_pb_bg_layout_light">
                        <div class="et_pb_text_inner">
                           <h1>Customer Details</h1>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="et_pb_row et_pb_row_1 et_pb_equal_columns et_pb_gutters2 et_pb_row_4col">
                  <div class="et_pb_column et_pb_column_1_2 et_pb_column_1  et_pb_css_mix_blend_mode_passthrough">
                     <div class="et_pb_module et_pb_image et_pb_image_0">
                        <span class="et_pb_image_wrap "><img loading="lazy" width="100%" src="<?php echo $_SESSION['img_src'];?>"></span>
                     </div>
                  </div>
                  <div class="et_pb_column et_pb_column_1_2 et_pb_column_2  et_pb_css_mix_blend_mode_passthrough">
                     <div class="et_pb_module et_pb_image et_pb_image_1">
                        <span class="et_pb_image_wrap "><img loading="lazy" src="<?php bloginfo('template_directory'); ?>/images/spice-shop-43.png" alt="" title="spice-shop-43" width="auto" height="auto"></span>
                     </div>
                     <div class="et_pb_module et_pb_text et_pb_text_1  et_pb_text_align_left et_pb_bg_layout_light">
                        <div class="et_pb_text_inner customer-detail-block">
                           <h3>Your Order Details:</h3>
                           <table>
                              <tr>
                                 <th>Product</th>
                                 <th>Total</th>
                              </tr>
                              <tr>
                                 <td><?php echo $_SESSION['productname'];?></td>
                                 <td><?php echo $_SESSION['currency'];?> <?php echo $_SESSION['product_amount'];?></td>
                              </tr>
                              <tr>
                                 <td>Shipping</td>
                                 <td><?php echo $_SESSION['currency'];?> <?php echo $_SESSION['shipping'];?></td>
                              </tr>
                              <tr>
                                 <td>GST</td>
                                 <td><?php echo $_SESSION['currency'];?> <?php echo $finalgst;?></td>
                              </tr>
                              <tr>
                                 <td>Total Amount</td>
                                 <td><?php echo $_SESSION['currency'];?> <?php echo $_SESSION['amount'];?></td>
                              </tr> 
                           </table>
                           <form method="post" action="/customerdetails/">
                              <div class="form-fieldset">
                                    <div class="field-wrapper">
<div>&nbsp;</div>
                                       <label>Apply Coupon : </label>  
                                       <?php if($_SESSION['title']!=''){?>
<div class="form-field"><input type="text" value="<?php echo $_SESSION['title'];?>" required class="input" name="couponvalue" disabled placeholder="Apply Coupon Here"></div>
                                       <?php }else{?>
<div class="form-field"><input type="text" value="<?php echo $_SESSION['title'];?>" required class="input" name="couponvalue" placeholder="Apply Coupon Here"></div>
                                       <?php }?>
                                       
                                    </div>
                                   <div class="et_pb_button_module_wrapper et_pb_button_0_wrapper et_pb_module addtocart_row">
<?php if($_SESSION['couponamt']!=''){?>
                                    <input type="submit" name="resetcoupon" class="et_pb_button et_pb_button_0 et_pb_bg_layout_light" value="Remove">
<?php }else{?>
                                    <input type="submit" name="couponSubmit" class="et_pb_button et_pb_button_0 et_pb_bg_layout_light" value="Apply">
<?php }?>


                                 </div>
                                 </div>
                           </form>
<?php if($_SESSION['sess_msg']!=''){?>
<div class="alert alert-danger mt-2"><?php echo $_SESSION['sess_msg'];?><?php $_SESSION['sess_msg']='';?></div>
<?php }?>
                           <div>&nbsp;</div>
                           <h3><?php the_title();?></h3>
                           <h3 class="price"><?php //echo $_SESSION['currency'];?> <?php //echo $_SESSION['amount'];?></h3>
                           <p><?php the_content();?></p>
                           <div class="customer_detail_form">
                              <h4>
                                 Enter Details
                                 <span class="product-qty">
                                    <!-- <input type="number" class="input" name="qty" value="<?php //echo $_SESSION['qty'];?>"> -->
                                    QTY <span><?php echo $_SESSION['qty'];?></span>
                                 </span>
                              </h4>
                              <form method="post" action="/pay/">
                                 <input type="hidden" name="">
                                 <?php for($i=0; $i<$_SESSION['qty']; $i++){?>
                                 <div class="form-fieldset">
                                    <div class="field-wrapper">
                                       <label>Email: </label>  
                                       <div class="form-field"><input type="email" class="input" name="email<?php echo $i;?>" placeholder="Enter Email"></div>
                                    </div>
                                    <div class="field-wrapper">
                                       <label>Phone: </label>  
                                       <div class="form-field"><input type="text" class="input" name="phone<?php echo $i;?>" placeholder="Enter Phone"></div>
                                    </div>
                                    <div class="field-wrapper">
                                       <input type="radio" name="primary_men" value="<?php echo $i;?>" <?php if($i==0){?>checked<?php }?>> <label>I am paying</label>
                                    </div>
                                 </div>
                                 <?php }?>
                                 <div class="et_pb_button_module_wrapper et_pb_button_0_wrapper et_pb_module addtocart_row">
                                    <input type="submit" name="submitnow" class="et_pb_button et_pb_button_0 et_pb_bg_layout_light" value="Submit Details">
                                 </div>
                              </form>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <!--div class="et_pb_section et_pb_section_2 et_pb_with_background et_section_regular">
               <div class="et_pb_row et_pb_row_3">
                  <div class="et_pb_column et_pb_column_4_4 et_pb_column_6  et_pb_css_mix_blend_mode_passthrough et-last-child">
                     <div class="et_pb_module et_pb_text et_pb_text_3 et_pb_text_align_center et_pb_bg_layout_dark et_had_animation">
                        <div class="et_pb_text_inner">
                           <h3>Join Today</h3>
                        </div>
                     </div>
                     <div class="et_pb_module et_pb_text et_pb_text_4 et_pb_text_align_center et_pb_bg_layout_dark et_had_animation">
                        <div class="et_pb_text_inner">
                           <h2>Spice Club</h2>
                        </div>
                     </div>
                     <div class="et_pb_module et_pb_image et_pb_image_4">
                        <span class="et_pb_image_wrap "><img loading="lazy" src="<?php bloginfo('template_directory'); ?>/images/spice-shop-64.png" alt="" title="spice-shop-64" width="auto" height="auto"></span>
                     </div>
                  </div>
               </div>
               <div class="et_pb_row et_pb_row_4">
                  <div class="et_pb_column et_pb_column_4_4 et_pb_column_7  et_pb_css_mix_blend_mode_passthrough et-last-child">
                     <div class="et_pb_button_module_wrapper et_pb_button_2_wrapper et_pb_button_alignment_center et_pb_module ">
                        <a class="et_pb_button et_pb_button_2 et_pb_bg_layout_light" href="/shop">Become a Spice Club Member</a>
                     </div>
                     <div class="et_pb_module et_pb_text et_pb_text_5  et_pb_text_align_center et_pb_bg_layout_light">
                        <div class="et_pb_text_inner">
                           <p>&nbsp;Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est</p>
                        </div>
                     </div>
                     <div class="et_pb_module et_pb_text et_pb_text_6  et_pb_text_align_center et_pb_bg_layout_light">
                        <div class="et_pb_text_inner">
                           <p>only $14 / mo</p>
                        </div>
                     </div>
                  </div>
               </div>
               </div>
               <div class="et_pb_section et_pb_section_3 et_pb_with_background et_section_regular">
               <div class="et_pb_row et_pb_row_5">
                  <div class="et_pb_column et_pb_column_1_2 et_pb_column_8  et_pb_css_mix_blend_mode_passthrough">
                     <div class="et_pb_module et_pb_text et_pb_text_7  et_pb_text_align_left et_pb_bg_layout_light">
                        <div class="et_pb_text_inner">
                           <h3>Address</h3>
                           <p><a href="#">5678 Extra Rd. #123 San Francisco, CA 96120.</a></p>
                        </div>
                     </div>
                     <div class="et_pb_module et_pb_text et_pb_text_8  et_pb_text_align_left et_pb_bg_layout_light">
                        <div class="et_pb_text_inner">
                           <h3>Phone Number</h3>
                           <p><a href="#">(255) 352-6258</a></p>
                        </div>
                     </div>
                     <div class="et_pb_module et_pb_text et_pb_text_9  et_pb_text_align_left et_pb_bg_layout_light">
                        <div class="et_pb_text_inner">
                           <h3>Open EveryDay</h3>
                           <p>8AM – 5PM</p>
                        </div>
                     </div>
                     <div class="et_pb_button_module_wrapper et_pb_button_3_wrapper  et_pb_module ">
                        <a class="et_pb_button et_pb_button_3 et_pb_bg_layout_light" href="">Visit The Shop</a>
                     </div>
                  </div>
                  <div class="et_pb_column et_pb_column_1_2 et_pb_column_9  et_pb_css_mix_blend_mode_passthrough et-last-child et_pb_column_empty"></div>
               </div>
               </div-->
         </div>
      </div>
   </div>
</div>
<div class="modal fade custom-modal" id="frmModal" tabindex="-1" role="dialog" aria-labelledby="frmModalLabel" aria-hidden="true">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title" id="frmModalLabel">Get in Touch</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <div class="modal-body">
            <div class="row">
               <div class="col-lg-10 offset-lg-1">
                  <div class="frm-holder mt-4">
                     <?php echo do_shortcode('[contact-form-7 id="218" title="Contact form 1"]');?>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<?php endwhile; ?>
<?php get_footer();?>