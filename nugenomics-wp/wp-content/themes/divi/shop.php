<?php /*Template Name: Shop Page*/

get_header();?>

<section class="content-section product-detail-section">
       <div class="container">
        <div class="block-title text-center mb-5">Shop Now</div>
          <div class="row">
            <div class="col-lg-6">
                 <div class="product-media text-center mb-5">
                    <figure>
                       <img src="<?php bloginfo('template_directory'); ?>/images/product-image.svg" class="mb-2" alt="">
                       <figcaption><strong>Health Report & 3-month<br> health plan</strong></figcaption>
                    </figure>
                 </div>
            </div>
            <div class="col-lg-6">
               <div class="product-detail">
                  <div class="block-title product-title fsize-sm mb-3">DNA Based Holistic Wellness Solution</div>
                  <div class="product-price mb-2">
                     <strong class="offerprice">Rs. 7,500</strong> <span class="old-price">Rs. 15,000</span>
                  </div>
                  <div class="offer-info clr-lightblue mb-3">Exclusive Launch Offer <strong>(50% Off)</strong></div>
                  <div class="addto-cart mb-3"><a href="#" class="button addtocart-button">Buy Now</a></div>
                  <div class="product-desc mb-2">
                     <p>Unleash your inner 'Awesome' with our unique wellness product. Receive a detailed health report which includes:</p>
                     <ul class="product-feature-list">
                        <li>Genetic analysis</li>
                        <li>Blood report</li>
                        <li>Lifestyle analysis</li>
                     </ul>
                     <p>and a comprehensive 3 month health plan with regular consultations.</p>
                  </div>
                  <div class="signup-link clr-lightblue"><strong><u>Sign up and get ready to rock!</u></strong></div>
               </div>
            </div>
          </div>
       </div>
    </section>
<?php get_footer();?>