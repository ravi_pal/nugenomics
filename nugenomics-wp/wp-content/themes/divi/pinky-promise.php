<?php /*Template Name: Pinky Promise*/

get_header();?>

<section class="herosection content-section pb-0" style="background-color: #a0b8db;">
       <div id="herobanner">
          <div class="page-title mb-3">No Dieting No Straving <br><span>This is life</span><br class="d-block d-sm-none"> Not a Regime</div>
          <div class="banner-img text-center anim-gif pl-3 pr-3">
             <img src="<?php bloginfo('template_directory'); ?>/images/gif/pinky-promise-banner.gif" width="720" alt="">
          </div>
       </div>
    </section>
    <section id="scrolling-blocks-wrapper">
       <div class="container">
          <div id="scrolling-blocks" class="pinkypromise-scrolling-blocks">
             <div class="pinned-content-block item-1 d-flex flex-wrap align-items-center">
                <div class="pinned-content">
                   <div class="block-title text-center mb-4">We Pinky Promise</div>
                 <div class="block-img text-center">
                      <img src="<?php bloginfo('template_directory'); ?>/images/gif/we-pinky-promise.gif" width="700" alt="">
                   </div>
                </div>
             </div>
             <div class="pinned-content-block item-2 d-flex flex-wrap align-items-center">
                <div class="pinned-content">
                   <div class="block-title fsize-md text-center mb-3">We do our Research</div>
                   <div class="pwrap fsize-1 text-center clr-light">
                    <p>We’re those nerds who have all citations ready even before others can build up their thesis argument.</p>
                 </div>
                 <div class="block-img text-center">
                      <img src="<?php bloginfo('template_directory'); ?>/images/gif/we-do-our-research.gif" width="300" alt="">
                   </div>
                </div>
             </div>
             <div class="pinned-content-block item-3 d-flex flex-wrap align-items-center">
                <div class="pinned-content">
                   <div class="block-title fsize-md text-center mb-3">Anonymity Assured</div>
                   <div class="pwrap fsize-1 text-center clr-light">
                    <p>All your information is always kept anonymous (even from your better half)</p>
                 </div>
                 <div class="block-img text-center">
                      <img src="<?php bloginfo('template_directory'); ?>/images/gif/amonymity.gif" width="300" alt="">
                   </div>
                </div>
             </div>
             <div class="pinned-content-block item-4 d-flex flex-wrap align-items-center">
                <div class="pinned-content">
                   <div class="block-title fsize-md text-center mb-3">We are Expert Approved</div>
                   <div class="pwrap fsize-1 text-center clr-light">
                    <p>All our work is approved by the who’s who of the industry. We can get them on call if you want.</p>
                 </div>
                 <div class="block-img text-center">
                      <img src="<?php bloginfo('template_directory'); ?>/images/gif/we-are-expert-approved.gif" width="400" alt="">
                   </div>
                </div>
             </div>
          </div>
       </div>
    </section>


    <script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/modernizr.custom.min.js"></script>
      <script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/gsap.min.js"></script>
      <script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/ScrollMagic.js"></script>
      <script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/animation.gsap.js"></script>
      <script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/scrollmagic-custom2.js"></script>
<?php get_footer();?>