<?php /*Template Name: About*/

get_header();?>

<section class="herosection content-section pb-0" style="background-color: <?php the_field('background_colour');?>;">
       <div id="herobanner">
          <div class="page-title mb-3"><?php the_field('page_heading');?></div>
          <div class="banner-img text-center anim-gif pl-3 pr-3">
             <img src="<?php the_field('banner'); ?>" width="720" alt="">
          </div>
       </div>
    </section>
    <section id="scrolling-blocks-wrapper">
       <div class="container">
          <div id="scrolling-blocks" class="aboutus-scrolling-blocks">
             <?php $s = 0;
               while ( have_rows('flexible_fields') ) : the_row(); 
                  if( get_row_layout() == 'slide_with_content' ): $s++; if($s == 11){ $s++; }?>

                    <div class="pinned-content-block item-<?php echo $s;?> d-flex flex-wrap align-items-center">
                        <div class="pinned-content">
                           <div class="block-title text-center mb-4"><?php the_sub_field('heading');?></div>
                           <div class="pwrap text-center clr-light">
                               <p><?php the_sub_field('description');?></p>
                           </div>
                        </div>
                     </div>
                    <?php
                  elseif(get_row_layout() == 'slide_with_image' ): $s++; if($s == 11){ $s++; }?>
                    <div class="pinned-content-block item-<?php echo $s;?> d-flex flex-wrap align-items-center">
                        <div class="pinned-content">
                            <div class="pwrap fsize-1 text-center clr-light mb-3"><p><?php the_sub_field('heading');?></p></div>
                           <div class="block-title fsize-md text-center mb-3"><?php the_sub_field('sub_heading');?></div>
                           <div class="block-img text-center">
                             <img src="<?php the_sub_field('image');?>" width="400" alt="">
                          </div>
                        </div>
                     </div>

                    <?php
                  endif;
               endwhile;
            ?>
             <!--div class="pinned-content-block item-1 d-flex flex-wrap align-items-center">
                <div class="pinned-content">
                   <div class="block-title text-center mb-4">About NuGenomics</div>
                   <div class="pwrap text-center clr-light">
                       <p>It’s rapid fire time! Since you’ve gotten this far, it’s only fair you get to know us and our thoughts behind NuGenomics a little bit better. Are you ready? 3...2...1...</p>
                   </div>
                </div>
             </div>
             <div class="pinned-content-block item-2 d-flex flex-wrap align-items-center">
                <div class="pinned-content">
                    <div class="pwrap fsize-1 text-center clr-light mb-3"><p>Who are we exactly?</p></div>
                   <div class="block-title fsize-md text-center mb-3">Four Nerds</div>
                   <div class="block-img text-center">
                     <img src="<?php bloginfo('template_directory'); ?>/images/gif/for-nerds.gif" width="400" alt="">
                  </div>
                </div>
             </div>
             <div class="pinned-content-block item-3 d-flex flex-wrap align-items-center">
                <div class="pinned-content">
                    <div class="pwrap fsize-1 text-center clr-light mb-3">
                        <p>What do you get when you put 4 nerds from<br class="d-none d-lg-block"> various fields in one room?</p>
                     </div>
                       <div class="block-title fsize-md text-center mb-3">Chaos (absolute chaos)</div>
                 <div class="block-img text-center">
                      <img src="<?php bloginfo('template_directory'); ?>/images/gif/chaos.gif" width="300" alt="">
                   </div>
                </div>
             </div>
             <div class="pinned-content-block item-4 d-flex flex-wrap align-items-center">
                <div class="pinned-content">
                    <div class="pwrap fsize-1 text-center clr-light mb-3">
                        <p>All from different fields, speaking different tongues.<br class="d-none d-lg-block"> Wondering what we found in common?</p>
                     </div>
                       <div class="block-title fsize-md text-center mb-3">Math!</div>
                 <div class="block-img text-center">
                      <img src="<?php bloginfo('template_directory'); ?>/images/gif/math.gif" width="420" alt="">
                   </div>
                </div>
             </div>
             <div class="pinned-content-block item-5 d-flex flex-wrap align-items-center">
                <div class="pinned-content">
                    <div class="pwrap fsize-1 text-center clr-light mb-3">
                        <p>What followed was countless days of research in labs.<br class="d-none d-lg-block"> All of us driven by one purpose - To bring about a...</p>
                     </div>
                       <div class="block-title fsize-md text-center mb-5">Health Revolution</div>
                 <div class="block-img text-center">
                      <img src="<?php bloginfo('template_directory'); ?>/images/gif/health-revolution.gif" width="600" alt="">
                   </div>
                </div>
             </div>
             <div class="pinned-content-block item-6 d-flex flex-wrap align-items-center">
                <div class="pinned-content">
                    <div class="pwrap fsize-1 text-center clr-light mb-3">
                        <p>The goal was to improve...</p>
                     </div>
                       <div class="block-title fsize-md text-center mb-4">Holistic well-being</div>
                 <div class="block-img text-center">
                      <img src="<?php bloginfo('template_directory'); ?>/images/gif/holostic.gif" width="300" alt="">
                   </div>
                </div>
             </div>
             <div class="pinned-content-block item-7 d-flex flex-wrap align-items-center">
                <div class="pinned-content">
                    <div class="pwrap fsize-1 text-center clr-light mb-3">
                        <p>Our curious minds were bubbling with many questions.<br class="d-none d-lg-block"> But guess where we finally found the answers?</p>
                     </div>
                       <div class="block-title fsize-md text-center mb-4">On the inside!</div>
                 <div class="block-img text-center">
                      <img src="<?php bloginfo('template_directory'); ?>/images/gif/on-the-inside.gif" width="230" alt="">
                   </div>
                </div>
             </div>
             <div class="pinned-content-block item-8 d-flex flex-wrap align-items-center">
                <div class="pinned-content">
                    <div class="pwrap fsize-1 text-center clr-light mb-3">
                        <p>The key to us really getting to know you.</p>
                     </div>
                       <div class="block-title fsize-md text-center mb-3">Your Genes.</div>
                 <div class="block-img text-center">
                      <img src="<?php bloginfo('template_directory'); ?>/images/gif/your-genes.gif" width="350" alt="">
                   </div>
                </div>
             </div>
             <div class="pinned-content-block item-9 d-flex flex-wrap align-items-center">
                <div class="pinned-content">
                    <div class="pwrap fsize-1 text-center clr-light mb-3">
                        <p>Your genes give us some guidelines.<br class="d-none d-lg-block"> These will help you…</p>
                     </div>
                       <div class="block-title fsize-md text-center mb-4">Unlock your maximum potential</div>
                 <div class="block-img text-center">
                      <img src="<?php bloginfo('template_directory'); ?>/images/gif/unlock.gif" width="400" alt="">
                   </div>
                </div>
             </div>
             <div class="pinned-content-block item-10 d-flex flex-wrap align-items-center">
                <div class="pinned-content">
                    <div class="pwrap fsize-1 text-center clr-light mb-3">
                        <p>We have a panel of nutrition experts that would assist you in the<br class="d-none d-lg-block"> flight from you to a better you. They have a fancy name.</p>
                     </div>
                       <div class="block-title fsize-md text-center mb-3">Nutri-Genomists</div>
                 <div class="block-img text-center">
                      <img src="<?php bloginfo('template_directory'); ?>/images/gif/nutrigeno.gif" width="360" alt="">
                   </div>
                </div>
             </div>
             <div class="pinned-content-block item-11 d-flex flex-wrap align-items-center">
                <div class="pinned-content">
                    <div class="pwrap fsize-1 text-center clr-light mb-3">
                        <p>We promise you won’t have to...</p>
                     </div>
                       <div class="block-title fsize-md text-center mb-3">Starve</div>
                 <div class="block-img text-center">
                      <img src="<?php //bloginfo('template_directory'); ?>/images/gif/starve.gif" width="350" alt="">
                   </div>
                </div>
             </div> 
             <div class="pinned-content-block item-12 d-flex flex-wrap align-items-center">
                <div class="pinned-content">
                    <div class="pwrap fsize-1 text-center clr-light mb-3">
                        <p>We will stick by you every step of the way.<br class="d-none d-lg-block"> We are in this...</p>
                     </div>
                       <div class="block-title fsize-md text-center mb-3">Together</div>
                 <div class="block-img text-center">
                      <img src="<?php bloginfo('template_directory'); ?>/images/gif/together_2.gif" width="300" alt="">
                   </div>
                </div>
             </div>
             <div class="pinned-content-block item-13 d-flex flex-wrap align-items-center">
                <div class="pinned-content">
                    <div class="pwrap fsize-1 text-center clr-light mb-3">
                        <p>Last question, are you ready to feel the best<br class="d-none d-lg-block"> version of yourself?</p>
                     </div>
                       <div class="block-title fsize-md text-center mb-3">There’s only one right answer ;) Yes!</div>
                 <div class="block-img text-center">
                      <img src="<?php bloginfo('template_directory'); ?>/images/gif/right-answer.gif" width="360" alt="">
                   </div>
                </div>
             </div-->
          </div>
       </div>
    </section>
<section class="content-section lightbg">
    <div class="container">
        <div class="block-title text-center mb-5">Meet The Team</div>
        <div class="team-member-container">
            <ul>
                <?php 
            $the_query = new WP_Query(array('post_type' => 'employee', 'orderby'=>'ID', 'order' => 'ASC', 'paged' => get_query_var('paged') ? get_query_var('paged') : 1 ));
            if($the_query->have_posts()): 
            while ($the_query -> have_posts()) : $the_query -> the_post();
            $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );  
            //$getsecondimage=$dynamic_featured_image->get_featured_images([$post->ID]);
            //$getimagesecondurl=$getsecondimage[0]['full'];
          ?>

            <li>
                <div class="team-member">
                    <div class="avatar mb-4 mt-3">
                        <img src="<?php echo $url;?>" alt="">
                    </div>
                    <div class="member-meta">
                        <h4 class="mb-3"><?php the_title();?></h4>
                        <?php the_field('skiils');?>
                    </div>
                    <div class="degination mt-4">
                        <?php the_content();?>
                    </div>
                </div>
            </li>

          <?php  endwhile; ?>
                                  <?php endif; 
          wp_reset_query();?>
                <!--li>
                    <div class="team-member">
                        <div class="avatar mb-4 mt-3">
                            <img src="<?php bloginfo('template_directory'); ?>/images/team1.png" alt="">
                        </div>
                        <div class="member-meta">
                        <h4 class="mb-3">Rahul Ranganathan</h4>
                        <p>- CSIR Research Fellow <br>
                            - MBA from XLRI Jamshedpur <br>
                            - 6+ years of experience across domains</p>
                        </div>
                        <div class="degination mt-4">
                            CoFounder – Molecular Biologist
                        </div>
                    </div>
                </li>
                 
                <li>
                    <div class="team-member">
                        <div class="avatar mb-4 mt-3">
                            <img src="<?php bloginfo('template_directory'); ?>/images/team2.png" alt="">
                        </div>
                        <div class="member-meta">
                        <h4 class="mb-3">Dr. Shanthi <br>Lakshmi Duraimani</h4>
                        <p>- Ph.D. in Mind-body genomics <br>
                            - 5 years+ experience in Nutrigenomics
                            </p>
                        </div>
                        <div class="degination mt-4">
                            Nutri-Genomist
                        </div>
                    </div>
                </li>
                <li>
                    <div class="team-member">
                        <div class="avatar mb-4 mt-3">
                            <img src="<?php bloginfo('template_directory'); ?>/images/team3.png" alt="">
                        </div>
                        <div class="member-meta">
                        <h4 class="mb-3">Dr. Balamurali AR</h4>
                        <p>- Post Doc in Machine Learning<br>
                            - Ph.D in Computer Science from IITB<br>
                            - 15+ years of experience
                            </p>
                        </div>
                        <div class="degination mt-4">
                            CoFounder – The Data Guy
                        </div>
                    </div>
                </li-->
            </ul>
        </div>
    </div>
</section>

<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/modernizr.custom.min.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/gsap.min.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/ScrollMagic.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/animation.gsap.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/scrollmagic-custom3.js"></script>

<?php get_footer();?>