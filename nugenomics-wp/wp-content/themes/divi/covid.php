<?php /*Template Name: Covid*/

get_header();?>

<link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/covid/css/covid.css">

<header class="landing-page-header">
         <div class="header-logo d-flex align-items-center">
            <a href="/">
                <img src="<?php bloginfo('template_directory'); ?>/images/logo-dark.png" alt="">
            </a>
         </div>
      </header>
      <section class="section-one pt-100 posrel">
         <div class="section-heading">
            <img src="<?php the_field('top_logo');?>" alt="">
         </div>
         <div class="section-img">
            <?php $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); 
            $getsecondimage=$dynamic_featured_image->get_featured_images([$post->ID]);
            $getimagesecondurl=$getsecondimage[0]['full'];?>
            <img src="<?php echo $url;?>" width="100%" alt="">
         </div>
         <?php $contentIcon = get_field('content_with_icon_');
         if($contentIcon):?>
         <div class="content-block-outer">
            <div class="container">
               <div class="row">
                  <div class="col-lg-8 offset-lg-2">
                     <div class="lefticon-content-block">
                        <img src="<?php echo $contentIcon['icon'];?>" class="icon" alt="">
                        <h3 class="clr-white hashtag"><?php echo $contentIcon['heading'];?></h3>
                        <div class="para-holder clr-white">
                           <p><?php echo $contentIcon['content'];?> 
                           </p>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      <?php endif;?>
      </section>

      <?php
         while ( have_rows('flexible_fields') ) : the_row();
            if( get_row_layout() == 'content_with_banner' ):
               get_template_part('template-parts/ContentWithBanner');
            elseif(get_row_layout() == 'listing_content_with_image'):
               get_template_part('template-parts/ListingContentWithImage');
            elseif(get_row_layout() == 'center_content_layout'):
               get_template_part('template-parts/CenterContentLayout');
            elseif(get_row_layout() == 'center_content_layout_2'):
               get_template_part('template-parts/CenterContentLayout2');
            elseif(get_row_layout() == 'listing_content_layout_2'):
               get_template_part('template-parts/ListingContentLayout2'); 
            elseif(get_row_layout() == 'center_content_with_button'):
               get_template_part('template-parts/CenterContentWithButton');         
            endif;
         endwhile;
      ?>      
     
      <!--section class="section-eight pt-100 pb-100">
         <div class="container">
            <div class="row">
               <div class="col-lg-8 offset-lg-2">
            <div class="content-block text-center">
               <div class="section-heading text-uppercase clr-white mb-4  fsize-md">Still unsure?<br>
                  Let us clarify any doubts you have.
               </div>
               <a href="#" class="btn-style btn-style-white" data-toggle="modal" data-target="#frmModal">Get in touch</a>
            </div>
            </div>
            </div>
         </div>
      </section-->

            <!-- Modal -->
<div class="modal fade custom-modal" id="frmModal" tabindex="-1" role="dialog" aria-labelledby="frmModalLabel" aria-hidden="true">
   <div class="modal-dialog" role="document">
     <div class="modal-content">
       <div class="modal-header">
         <h5 class="modal-title" id="frmModalLabel">Get in Touch</h5>
         <button type="button" class="close" data-dismiss="modal" aria-label="Close">
           <span aria-hidden="true">&times;</span>
         </button>
       </div>
       <div class="modal-body">
         <div class="row">
            <div class="col-lg-10 offset-lg-1">
               <div class="frm-holder mt-4">
<?php echo do_shortcode('[contact-form-7 id="218" title="Contact form 1"]');?>
               </div>
            </div>
         </div>
       </div>
     </div>
   </div>
 </div>

<?php get_footer();?>