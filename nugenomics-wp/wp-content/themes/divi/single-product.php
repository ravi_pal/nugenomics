<?php 
session_start();
get_header();
$_SESSION['couponamt']='';   
$_SESSION['title']='';
?>
<?php  while ( have_posts() ) : the_post(); 
$url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); 
?>
<section class="content-section product-detail-section">
       <div class="container">
        <div class="block-title text-center mb-5">Shop Now</div>
          <div class="row">
            <div class="col-lg-6">
                 <div class="product-media text-center mb-5">
                    <figure>
                       <img src="<?php bloginfo('template_directory'); ?>/images/product-image.svg" class="mb-2" alt="">
                       <figcaption><strong>Health Report & 3-month<br> health plan</strong></figcaption>
                    </figure>
                 </div>
            </div>
            <div class="col-lg-6">
               <div class="product-detail">
                  <div class="block-title product-title fsize-sm mb-3"><?php the_title();?></div>
                  <div class="product-price mb-2">
                     <strong class="offerprice"><?php the_field('currency');?> <?php the_field('price_included_gst');?></strong> <span class="old-price"><?php the_field('currency');?> <?php the_field('offer_price');?></span>
                  </div>
                  <div class="offer-info clr-lightblue mb-3">Exclusive Launch Offer <strong>(50% Off)</strong></div>

   <p class="mb-2"><em>Please write quantity.</em></p>


                                  <form method="post" action="/customerdetails/" id="frm" class="d-flex align-items-center">
<input type="hidden" name="productname" value="<?php the_title();?>">
<input type="hidden" name="img_src" value="<?php echo $url;?>">
   <div class="qty-input-wrap mb-3">
   <input type="number" name="qty" value="1">
</div>
                  <div class="addto-cart mb-3"><a id="submit_id" href="javascript:void(0)" class="button addtocart-button">Buy Now</a></div>
</form>

                  <div class="product-desc mb-2">
                     <p>Unleash your inner 'Awesome' with our unique wellness product. Receive a detailed health report which includes:</p>
                     <ul class="product-feature-list">
                        <li>Genetic analysis</li>
                        <li>Blood report</li>
                        <li>Lifestyle analysis</li>
                     </ul>
                     <p>and a comprehensive 3 month health plan with regular consultations.</p>
                  </div>
                  <div class="signup-link clr-lightblue"><strong><u>Sign up and get ready to rock!</u></strong></div>


               </div>
            </div>
          </div>
       </div>
    </section>
<?php endwhile; ?>
<div class="modal fade custom-modal" id="frmModal" tabindex="-1" role="dialog" aria-labelledby="frmModalLabel" aria-hidden="true">
   <div class="modal-dialog" role="document">
     <div class="modal-content">
       <div class="modal-header">
         <h5 class="modal-title" id="frmModalLabel">Get in Touch</h5>
         <button type="button" class="close" data-dismiss="modal" aria-label="Close">
           <span aria-hidden="true">&times;</span>
         </button>
       </div>
       <div class="modal-body">
         <div class="row">
            <div class="col-lg-10 offset-lg-1">
               <div class="frm-holder mt-4">
<?php echo do_shortcode('[contact-form-7 id="218" title="Contact form 1"]');?>
               </div>
            </div>
         </div>
       </div>
     </div>
   </div>
 </div>
<script type="text/javascript">
var form = document.getElementById("frm");
document.getElementById("submit_id").addEventListener("click", function () {
  form.submit();
});
</script>
<?php get_footer();?>