<?php /*Template Name: How It Works*/

get_header();?>

<section class="herosection content-section pb-0" style="background-color: #a0b8db;">
       <div id="herobanner">
          <div class="page-title">No Dieting No Straving <br><span>This is life</span><br class="d-block d-sm-none"> Not a Regime</div>
          <div class="banner-img text-center anim-gif pl-3 pr-3">
             <img src="<?php bloginfo('template_directory'); ?>/images/gif/howitworks-banner.gif" width="720" alt="">
          </div>
       </div>
    </section>
    <section id="scrolling-blocks-wrapper">
       <div class="container">
          <div id="scrolling-blocks" class="howitworks-scrolling-blocks">
             <div class="pinned-content-block item-1 d-flex flex-wrap align-items-center">
                <div class="pinned-content">
                   <div class="block-title fsize-md text-center mb-3">Introductions</div>
                   <div class="pwrap text-center clr-light">
                    <p>To start off, you talk and we listen. You introduce us to yourself.</p>
                 </div>
                 <div class="block-img text-center">
                      <img src="<?php bloginfo('template_directory'); ?>/images/gif/introductions.gif" width="300" alt="">
                   </div>
                </div>
             </div>
             <div class="pinned-content-block item-2 d-flex flex-wrap align-items-center">
                <div class="pinned-content">
                   <div class="block-title fsize-md text-center mb-3">Needlework</div>
                   <div class="pwrap text-center clr-light">
                    <p>We take a little blood to take a closer look & cross-reference with our database. With this info we will provide a Comprehensive Health Profile- Whatever your genes have told us. This will include a thorough blood report & an analysis of your genetic parameters.</p>
                 </div>
                 <div class="block-img text-center">
                      <img src="<?php bloginfo('template_directory'); ?>/images/gif/needelwork.gif" width="300" alt="">
                   </div>
                </div>
             </div>
             <div class="pinned-content-block item-3 d-flex flex-wrap align-items-center">
                <div class="pinned-content">
                   <div class="block-title fsize-md text-center mb-3">Lifestyle Analysis</div>
                   <div class="pwrap text-center clr-light">
                    <p>Everything you do has an impact on your health. We get into the nitty-gritties to see what we can tweak.</p>
                 </div>
                 <div class="block-img text-center">
                      <img src="<?php bloginfo('template_directory'); ?>/images/gif/lifestyle-and-analytics.gif" width="300" alt="">
                   </div>
                </div>
             </div>
             <div class="pinned-content-block item-4 d-flex flex-wrap align-items-center">
                <div class="pinned-content">
                   <div class="block-title fsize-md text-center mb-3">Recommendations</div>
                   <div class="pwrap text-center clr-light">
                    <p>It’s game time! Based on your health profile and lifestyle we tailor our recommendations to help you get maximum results. We provide diet guidance, lifestyle plan, the works. We’ll guide you through it all.</p>
                 </div>
                 <div class="block-img text-center">
                      <img src="<?php bloginfo('template_directory'); ?>/images/gif/recommandations.gif" width="300" alt="">
                   </div>
                </div>
             </div>
             <div class="pinned-content-block item-5 d-flex flex-wrap align-items-center">
                <div class="pinned-content">
                   <div class="block-title fsize-md text-center mb-3">Getting started</div>
                   <div class="pwrap text-center clr-light">
                    <p>Baby steps, in phase I, nothing drastic. From months 1-2 we introduce the smaller changes called micro-recommendations in your diet and lifestyle to let your body get accustomed to the 'NuWay'.</p>
                 </div>
                 <div class="block-img text-center mt-n3">
                      <img src="<?php bloginfo('template_directory'); ?>/images/gif/getting-startted.gif" width="300" alt="">
                   </div>
                   <div class="key-feature-4 clr-light">
                       <p>You may notice-</p>
                       <ul>
                           <li>A reduction in bloating</li>
                           <li>Improvement in digestive health</li>
                           <li>Higher energy levels</li>
                        <li>And an overall more fun outlook on your day.</li>
                       </ul>
                   </div>
                </div>
             </div>
             <div class="pinned-content-block item-6 d-flex flex-wrap align-items-center">
                <div class="pinned-content">
                   <div class="block-title fsize-md text-center mb-3">Amping it up</div>
                   <div class="pwrap text-center clr-light">
                    <p>Here we enter phase II towards the 'NuYou'. Those small changes to your day are gathering speed now and you can see visible changes from months 3-5.</p>
                 </div>
                 <div class="block-img text-center mt-n3">
                      <img src="<?php bloginfo('template_directory'); ?>/images/gif/amping-it-up.gif" width="300" alt="">
                   </div>
                   <div class="key-feature-4 clr-light">
                       <p>You may notice-</p>
                       <ul>
                           <li>Visible changes in body fat levels</li>
                           <li>Improvement in blood profile</li>
                           <li>That you’re lighter on your feet</li>
                           <li>And your days just keep getting better and better.</li>
                        </ul>
                   </div>
                </div>
             </div>
             <div class="pinned-content-block item-7 d-flex flex-wrap align-items-center">
                <div class="pinned-content">
                   <div class="block-title fsize-md text-center mb-3">Become limitless</div>
                   <div class="pwrap text-center clr-light">
                    <p>Six months in and you have transformed into the super healthy, super energetic brand new YOU! Your lifestyle is where it should be and your health, as they say, is in the pink of it!</p>
                 </div>
                 <div class="block-img text-center mt-n4">
                      <img src="<?php bloginfo('template_directory'); ?>/images/gif/become-limitless.gif" width="300" alt="">
                   </div>
                   <div class="key-feature-4 clr-light">
                       <p>You may notice-</p>
                       <ul>
                           <li>Increased muscle mass</li>
                           <li>Greater mobility</li>
                           <li>Lower stress & fatigue levels</li>
                           <li>Improved immunity</li>
                           <li>And that your body is running like a well-oiled machineready to take over the world (and we’re here for it)!</li>
                        </ul>
                   </div>
                </div>
             </div>
             <div class="pinned-content-block item-8 d-flex flex-wrap align-items-center">
                <div class="pinned-content">
                   <div class="block-title fsize-md text-center mb-3">Tracking & support</div>
                   <div class="pwrap text-center clr-light">
                    <p>We stand by you carry on until we reach your goals. Picking you up, pushing you & running with you till you are satisfied.</p>
                 </div>
                 <div class="block-img text-center">
                      <img src="<?php bloginfo('template_directory'); ?>/images/gif/tracking-and-support.gif" width="300" alt="">
                   </div>
                </div>
             </div>
          </div>
       </div>
    </section>


    <script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/modernizr.custom.min.js"></script>
      <script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/gsap.min.js"></script>
      <script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/ScrollMagic.js"></script>
      <script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/animation.gsap.js"></script>
      <script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/scrollmagic-custom1.js"></script>
<?php get_footer();?>