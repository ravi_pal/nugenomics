jQuery(function() {
    // define controller
    var controller = new ScrollMagic.Controller();
       var pinScene01 = new TimelineMax()
                  .fromTo(jQuery('#scrolling-blocks .item-1'), 0.1, {opacity:0, y: '+=20'}, {opacity:1, y: 0, ease:Power1.easeOut}, '-=0.02')
                  .to(jQuery('#scrolling-blocks .item-1'), 0.1, {opacity:0, ease:Power1.easeOut}, '+=0.3')
                  .fromTo(jQuery('#scrolling-blocks .item-2'), 0.1, {opacity:0, y: '+=20'}, {opacity:1, y: 0, ease:Power1.easeOut}, '-=0.02')
                  .to(jQuery('#scrolling-blocks .item-2'), 0.1, {opacity:0, ease:Power1.easeOut}, '+=0.3')
                  .fromTo(jQuery('#scrolling-blocks .item-3'), 0.1, {opacity:0, y: '+=20'}, {opacity:1, y: 0, ease:Power1.easeOut}, '-=0.02')
                  .to(jQuery('#scrolling-blocks .item-3'), 0.1, {opacity:0, ease:Power1.easeOut}, '+=0.3')
                  .fromTo(jQuery('#scrolling-blocks .item-4'), 0.1, {opacity:0, y: '+=20'}, {opacity:1, y: 0, ease:Power1.easeOut}, '-=0.02')
                  .to(jQuery('#scrolling-blocks .item-4'), 0.1, {opacity:0, ease:Power1.easeOut}, '+=0.3')
                  .fromTo(jQuery('#scrolling-blocks .item-5'), 0.1, {opacity:0, y: '+=20'}, {opacity:1, y: 0, ease:Power1.easeOut}, '-=0.02')
                  .to(jQuery('#scrolling-blocks .item-5'), 0.1, {opacity:0, ease:Power1.easeOut}, '+=0.3')
                  .fromTo(jQuery('#scrolling-blocks .item-6'), 0.1, {opacity:0, y: '+=20'}, {opacity:1, y: 0, ease:Power1.easeOut}, '-=0.02')
                  .to(jQuery('#scrolling-blocks .item-6'), 0.1, {opacity:0, ease:Power1.easeOut}, '+=0.3')
                  .fromTo(jQuery('#scrolling-blocks .item-7'), 0.1, {opacity:0, y: '+=20'}, {opacity:1, y: 0, ease:Power1.easeOut}, '-=0.02')
                  .to(jQuery('#scrolling-blocks .item-7'), 0.1, {opacity:0, ease:Power1.easeOut}, '+=0.3')
                  .fromTo(jQuery('#scrolling-blocks .item-8'), 0.1, {opacity:0, y: '+=20'}, {opacity:1, y: 0, ease:Power1.easeOut}, '-=0.02')
                  .to(jQuery('#scrolling-blocks .item-8'), 0.1, {opacity:1, ease:Power1.easeOut}, '+=0.3')
          
          new ScrollMagic.Scene({
             triggerElement: '#scrolling-blocks-wrapper',
             triggerHook: 0,
             duration: '800%'
          })
          .setPin('#scrolling-blocks-wrapper')
          .setTween(pinScene01)
          .addTo(controller);
 });

 jQuery(window).scroll(function(event){
            var opc1 = jQuery('#scrolling-blocks .item-5').css('opacity');
            var opc2 = jQuery('#scrolling-blocks .item-6').css('opacity');
            var opc3 = jQuery('#scrolling-blocks .item-7').css('opacity');

            if(opc1 > 0){
             jQuery('#scrolling-blocks .item-5 .key-feature-4 ul').addClass('fadein-list')
            }else{
             jQuery('#scrolling-blocks .item-5 .key-feature-4 ul').removeClass('fadein-list')
            }
            if(opc2 > 0){
             jQuery('#scrolling-blocks .item-6 .key-feature-4 ul').addClass('fadein-list')
            }else{
             jQuery('#scrolling-blocks .item-6 .key-feature-4 ul').removeClass('fadein-list')
            }
            if(opc3 > 0){
             jQuery('#scrolling-blocks .item-7 .key-feature-4 ul').addClass('fadein-list')
            }else{
             jQuery('#scrolling-blocks .item-7 .key-feature-4 ul').removeClass('fadein-list')
            }
        });