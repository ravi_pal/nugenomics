jQuery(function() {
    // define controller
    var controller = new ScrollMagic.Controller();
       var pinScene01 = new TimelineMax()
          .fromTo(jQuery('#scrolling-blocks .item-1'), 0.1, {opacity:0, y: '+=20'}, {opacity:1, y: 0, ease:Power1.easeOut}, '-=0.02')
          .to(jQuery('#scrolling-blocks .item-1'), 0.1, {opacity:0, ease:Power1.easeOut}, '+=0.1')
          .fromTo(jQuery('#scrolling-blocks .item-2'), 0.1, {opacity:0, y: '+=20'}, {opacity:1, y: 0, ease:Power1.easeOut}, '-=0.02')
          .to(jQuery('#scrolling-blocks .item-2'), 0.1, {opacity:0, ease:Power1.easeOut}, '+=0.1')
          .fromTo(jQuery('#scrolling-blocks .item-3'), 0.1, {opacity:0, y: '+=20'}, {opacity:1, y: 0, ease:Power1.easeOut}, '-=0.02')
          .to(jQuery('#scrolling-blocks .item-3'), 0.1, {opacity:0, ease:Power1.easeOut}, '+=0.1')
          .fromTo(jQuery('#scrolling-blocks .item-4'), 0.1, {opacity:0, y: '+=20'}, {opacity:1, y: 0, ease:Power1.easeOut}, '-=0.02')
          .to(jQuery('#scrolling-blocks .item-4'), 0.1, {opacity:1, ease:Power1.easeOut}, '+=0.1')
                           
          new ScrollMagic.Scene({
             triggerElement: '#scrolling-blocks-wrapper',
             triggerHook: 0,
             duration: '400%'
          })
          .setPin('#scrolling-blocks-wrapper')
          .setTween(pinScene01)
          .addTo(controller);
 });