jQuery(function() {
            // define controller
            var controller = new ScrollMagic.Controller();
               var pinScene01 = new TimelineMax()
                  .fromTo(jQuery('#scrolling-blocks .item-1'), 0.1, {opacity:0, y: '+=20'}, {opacity:1, y: 0, ease:Power1.easeOut}, '-=0.02')
                  .to(jQuery('#scrolling-blocks .item-1'), 0.1, {opacity:0, ease:Power1.easeOut}, '+=0.3')
                  .fromTo(jQuery('#scrolling-blocks .item-2'), 0.1, {opacity:0, y: '+=20'}, {opacity:1, y: 0, ease:Power1.easeOut}, '-=0.02')
                  .to(jQuery('#scrolling-blocks .item-2'), 0.1, {opacity:0, ease:Power1.easeOut}, '+=0.3')
                  .fromTo(jQuery('#scrolling-blocks .item-3'), 0.1, {opacity:0, y: '+=20'}, {opacity:1, y: 0, ease:Power1.easeOut}, '-=0.02')
                  .to(jQuery('#scrolling-blocks .item-3'), 0.1, {opacity:0, ease:Power1.easeOut}, '+=0.3')
                  .fromTo(jQuery('#scrolling-blocks .item-4'), 0.1, {opacity:0, y: '+=20'}, {opacity:1, y: 0, ease:Power1.easeOut}, '-=0.02')
                  .to(jQuery('#scrolling-blocks .item-4'), 0.1, {opacity:0, ease:Power1.easeOut}, '+=0.3')
                  .fromTo(jQuery('#scrolling-blocks .item-5'), 0.1, {opacity:0, y: '+=20'}, {opacity:1, y: 0, ease:Power1.easeOut}, '-=0.02')
                  .to(jQuery('#scrolling-blocks .item-5'), 0.1, {opacity:0, ease:Power1.easeOut}, '+=0.3')
               
                  .fromTo(jQuery('#scrolling-blocks .item-6'), 0.1, {opacity:0, y: '+=20'}, {opacity:1, y: 0, ease:Power1.easeOut}, '-=0.02')
                  .to(jQuery('#scrolling-blocks .item-6'), 0.1, {opacity:0, ease:Power1.easeOut}, '+=0.3')
                  .fromTo(jQuery('#scrolling-blocks .item-7'), 0.1, {opacity:0, y: '+=20'}, {opacity:1, y: 0, ease:Power1.easeOut}, '-=0.02')
                  .to(jQuery('#scrolling-blocks .item-7'), 0.1, {opacity:0, ease:Power1.easeOut}, '+=0.3')
                  
                  .fromTo(jQuery('#scrolling-blocks .item-8'), 0.1, {opacity:0, y: '+=20'}, {opacity:1, y: 0, ease:Power1.easeOut}, '-=0.02')
                  .to(jQuery('#scrolling-blocks .item-8'), 0.1, {opacity:0, ease:Power1.easeOut}, '+=0.3')
                  .fromTo(jQuery('#scrolling-blocks .item-9'), 0.1, {opacity:0, y: '+=20'}, {opacity:1, y: 0, ease:Power1.easeOut}, '-=0.02')
                  .to(jQuery('#scrolling-blocks .item-9'), 0.1, {opacity:1, ease:Power1.easeOut}, '+=0.3')
                  
                  new ScrollMagic.Scene({
                     triggerElement: '#scrolling-blocks-wrapper',
                     triggerHook: 0,
                     duration: '700%'
                  })
                  .setPin('#scrolling-blocks-wrapper')
                  .setTween(pinScene01)
                  .addTo(controller);
         });



         var lastScrollTop = 0;
         jQuery(window).scroll(function(event){
            var st = jQuery(this).scrollTop();

            var opc = jQuery('#scrolling-blocks .item-1').css('opacity');
            var opc1 = jQuery('#scrolling-blocks .item-6').css('opacity');
            var opc2 = jQuery('#scrolling-blocks .item-8').css('opacity');
           
            var opc3 = jQuery('#scrolling-blocks .item-5').css('opacity');
            var opc4 = jQuery('#scrolling-blocks .item-7').css('opacity');
            var opc5 = jQuery('#scrolling-blocks .item-9').css('opacity');

            if(opc4 > 0){
                  jQuery('.key-feature-2 ul').addClass('fadein-list')
               }else{
                  jQuery('.key-feature-2 ul').removeClass('fadein-list')
               }
               if(opc5 > 0){
                  jQuery('.key-feature-3 ul').addClass('fadein-list')
               }else{
                  jQuery('.key-feature-3 ul').removeClass('fadein-list')
               }

            if (st > lastScrollTop){
               // downscroll code
               if(opc > 0){
                     jQuery('.anchor-list').removeClass('active-2')
                     jQuery('.anchor-list').removeClass('active-3')
                     jQuery('.anchor-list').addClass('active-1')
               } else if (opc1 > 0){
                     jQuery('.anchor-list').removeClass('active-1')
                     jQuery('.anchor-list').removeClass('active-3')
                     jQuery('.anchor-list').addClass('active-2')
               }else if (opc2 > 0){
                     jQuery('.anchor-list').removeClass('active-1')
                     jQuery('.anchor-list').removeClass('active-2')
                     jQuery('.anchor-list').addClass('active-3')
               }
            } else {
               // upscroll code
               if(opc3 > 0){
                     jQuery('.anchor-list').removeClass('active-2')
                     jQuery('.anchor-list').removeClass('active-3')
                     jQuery('.anchor-list').addClass('active-1')
               } else if (opc4 > 0){
                     jQuery('.anchor-list').removeClass('active-1')
                     jQuery('.anchor-list').removeClass('active-3')
                     jQuery('.anchor-list').addClass('active-2')
               }else if (opc5 > 0){
                     jQuery('.anchor-list').removeClass('active-1')
                     jQuery('.anchor-list').removeClass('active-2')
                     jQuery('.anchor-list').addClass('active-3')
               }
            }
            lastScrollTop = st;
         });