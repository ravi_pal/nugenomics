jQuery(document).ready(function(){


// jQuery('#menu-item-413').find('a').attr('data-toggle', 'modal');
  //      jQuery('#menu-item-413').find('a').attr('data-target', '#frmModal');

    var kursorx = new kursor({
        type: 2,
        color: 'rgba(200,100,0)',
    });

    jQuery('.close-menu').on('click', function () {
        jQuery('body').removeClass('menuactive')
    });

    if(jQuery(window).width() < 1199){
        jQuery('.open-menu').on('click', function () {
            jQuery('body').toggleClass('menuactive')
        });
    }else{
        jQuery('.open-menu').on('mouseover', function () {
            jQuery('body').addClass('menuactive')
        });
        jQuery('#page-container').on('mouseout', function () {
            jQuery('body').removeClass('menuactive')
        });
} 



jQuery('#heroslider').slick({
    arrows:false,
    dots:true,
    infinite: true,
    speed: 300,
    autoplay:true,
    slidesToShow: 1,
    slidesToScroll: 1
});
jQuery('#slider-1').slick({
    arrows:true,
    dots:false,
    infinite: true,
    speed: 300,
    autoplay:true,
    slidesToShow: 3,
    slidesToScroll: 1,
    prevArrow:'<button type="button" class="slick-prev slick-arrow"><i class="fa fa-chevron-left"></i></button>',
    nextArrow:'<button type="button" class="slick-next slick-arrow"><i class="fa fa-chevron-right"></i></button>',
    responsive: [{
            breakpoint: 1199,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 1
            }
        },
        {
            breakpoint: 991,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 1
            }
        },
        {
            breakpoint: 767,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1
            }
        }
    ]
});
jQuery('#slider-2').slick({
    arrows:true,
    dots:false,
    infinite: true,
    autoplay:true,
    speed: 300,
    slidesToShow: 3,
    slidesToScroll: 1,
    prevArrow:'<button type="button" class="slick-prev slick-arrow"><i class="fa fa-chevron-left"></i></button>',
    nextArrow:'<button type="button" class="slick-next slick-arrow"><i class="fa fa-chevron-right"></i></button>',
   
    responsive: [{
        breakpoint: 1199,
        settings: {
            slidesToShow: 3,
            slidesToScroll: 1
        }
    },
    {
        breakpoint: 991,
        settings: {
            slidesToShow: 2,
            slidesToScroll: 1,
            arrows:false,
            dots:true,
        }
    },
    {
        breakpoint: 767,
        settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows:false,
            dots:true,
        }
    }
]
});

if(jQuery(window).width() < 767){
    jQuery('.steps-container').slick({
        arrows:false,
        dots:true,
        autoplay:true,
        infinite: true,
        speed: 300,
        slidesToShow: 1,
        slidesToScroll: 1,
    });
}  

});


window.onorientationchange = function(){
   window.location.reload();
}
