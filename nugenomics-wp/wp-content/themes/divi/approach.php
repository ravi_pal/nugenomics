<?php /*Template Name: Approach*/

get_header();?>

<section class="herosection content-section" style="background-color: #f47e7a;">
       <div id="herobanner">
          <div class="page-title mb-5"><?php the_field('page_heading');?></div>
          <div class="banner-img text-center anim-gif">
             <img src="<?php the_field('top_banner');?>" width="860" alt="">
          </div>
       </div>
    </section>
    <section id="scrolling-blocks-wrapper">
         <?php $headingGroup = get_field('heading_group');
         if($headingGroup):?>
        <div class="anchor-list">
            <ul class="d-flex flex-wrap">
                <li><?php echo $headingGroup['heading_1'];?></li>
                <li><?php echo $headingGroup['heading_2'];?></li>
                <li><?php echo $headingGroup['heading_3'];?></li>
            </ul>
        </div>
     <?php endif;?>
       <div class="container">
          <div id="scrolling-blocks">

            <?php 
               while ( have_rows('slide_layouts') ) : the_row();
                  if( get_row_layout() == 'slide_1' ):
                     get_template_part('template-parts/SlideLayout/slide1');
                  elseif(get_row_layout() == 'slide_2' ):  
                     get_template_part('template-parts/SlideLayout/slide2');
                  elseif(get_row_layout() == 'slide_3' ): 
                     get_template_part('template-parts/SlideLayout/slide3');   
                  elseif(get_row_layout() == 'slide_4'):
                     get_template_part('template-parts/SlideLayout/slide4');
                  elseif(get_row_layout() == 'slide_5'):
                     get_template_part('template-parts/SlideLayout/slide5'); 
                  elseif(get_row_layout() == 'slide_6'):
                     get_template_part('template-parts/SlideLayout/slide6'); 
                  elseif(get_row_layout() == 'slide_7'):
                     get_template_part('template-parts/SlideLayout/slide7'); 
                  elseif(get_row_layout() == 'slide_8'):
                     get_template_part('template-parts/SlideLayout/slide8'); 
                  elseif(get_row_layout() == 'slide_9'):
                     get_template_part('template-parts/SlideLayout/slide9');           
                  endif;
               endwhile;
            ?>

          </div>
         
          

       </div>
    </section>
<div class="modal fade custom-modal" id="frmModal" tabindex="-1" role="dialog" aria-labelledby="frmModalLabel" aria-hidden="true">
   <div class="modal-dialog" role="document">
     <div class="modal-content">
       <div class="modal-header">
         <h5 class="modal-title" id="frmModalLabel">Get in Touch</h5>
         <button type="button" class="close" data-dismiss="modal" aria-label="Close">
           <span aria-hidden="true">&times;</span>
         </button>
       </div>
       <div class="modal-body">
         <div class="row">
            <div class="col-lg-10 offset-lg-1">
               <div class="frm-holder mt-4">
<?php echo do_shortcode('[contact-form-7 id="218" title="Contact form 1"]');?>
               </div>
            </div>
         </div>
       </div>
     </div>
   </div>
 </div>

    <script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/modernizr.custom.min.js"></script>
      <script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/gsap.min.js"></script>
      <script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/ScrollMagic.js"></script>
      <script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/animation.gsap.js"></script>
      <script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/scrollmagic-custom.js"></script>
<?php get_footer();?>