<?php /*Template Name: Privacy Policy*/

get_header();?>

<section class="content-section">
        <div class="container">
        <div class="block-title text-center mb-40">Privacy Policy</div>
    <div class="content-container">
    <p>Welcome to NuGenomics. This Privacy Policy is subject to the Terms of Use displayed on <a href="www.nugenomics.in">www.NuGenomics.in</a> ("The Website") and NuGenomics web application ("<strong>NuGenomics/App</strong>"). The said Website and App are owned and operated by Answer Genomics Private Limited, a company incorporated under the provisions of Companies Act, 2013 ("<strong>The Company</strong>", "<strong>we</strong>", "<strong>us</strong>"). The Website and App function as an intermediary as per Section 2 of Information Technology Act, 2000. We at NuGenomics are strongly committed to your right to privacy. We have drawn out a privacy statement with regard to the information we collect from you in accordance with the provisions of Rule 3(1) of the Information Technology (Intermediaries Guidelines) Rules, 2011. You acknowledge that you are disclosing information voluntarily. By accessing /using this Website/App and/or by providing your information, you consent to the collection and use of the information you disclose on the Website/App in accordance with this Privacy Policy. </p>
    <h4>NuGenomics/Website shall collect Personal information/data from:</h4>
<ul>
    <li>Free Membership/Visitor to the Website/App or any pages thereof ("<strong>Visitor</strong>"); or</li>
    <li>A person who has One-time Paid Membership or Paid Subscription Membership defined in terms of use ("<strong>Member</strong>") to avail our products and Services ("<strong>Services</strong>").</li>
    <li>The type Personal data collected/stored/transferred by NuGenomics/Website varies based on the above mentioned capacity in which you use the Website/App/avail of our services</li>
    <li>In the event such aforementioned Visitors/Members are under the age of 18, they must seek the consent and assistance of their parents before providing any personal information about themselves, their parents, or other family members on our Website/App.</li>
</ul>
<h4>Purpose of collection and usage of information from you</h4>
<ul>
    <li>We collect information from you to enable Visitors to partially generate NuGenomics Report as they go in NuGenomics application flow and enable Members to generate a complete NuGenomics Report as the case may be.</li>
    <li>The information provided by you shall also be used to prepare a suitable diet plan and provide activity recommendations.</li>
</ul>
<h4>Types of Personal Information gathered by the Website/App</h4>
<ul>
    <li>Personal information (PI) - means any information relating to an identified or identifiable natural person including common identifiers such as a name, an identification number, location data, an online identifier or one or more factors specific to the physical, physiological, genetic, mental, economic, cultural or social identity of that natural person and any other information that is so categorized by applicable laws. ("Personal Information/Data")</li>
    <li>Website/App gathers following types of information
        <ul>
            <li>Information submitted by you</li>
            <li>Information not directly submitted by you</li>
        </ul>
    </li>
</ul>
<h4>Information submitted by you</h4>
<ul>
    <li>For the purposes of acquiring the NuGenomics Report, a Member/ Visitor may provide information such as their name, date of birth, gender, height, weight, waist and hip circumference, ethnicity, diet and activity data, food preferences, medical conditions (if any), family disease history, health goals, diagnostic data etc</li>
    <li>Credit card/debit card/other payment mode information to facilitate payments for our Services;</li>
    <li>Information that may be submitted by you voluntarily while participating in surveys contest, promotions or events.</li>
    <li>Any details that you may have shared with our customer care team. (This information is used to monitor or for training purposes and to ensure a better service).</li>
</ul>
<h4>Information not directly submitted by you</h4>
<ul>
    <li>User Activity: We collect information about your activity on our Website/App, such as date and time you logged in, searches, clicks and pages visited by you etc</li>
    <li>Device Information: We collect information from and about your device(s) such as IP address, device ID and type, device-specification, browser type and version, operating system, identifiers associated with cookies or other technologies that may uniquely identify your device or browser.</li>
    <li>Your location unless you deactivate location services in the relevant section of the Website/App</li>
    <li>How you behave in the relevant product environment and use the features;</li>
    <li>SMS Permission: We need SMS permission for authenticating transactions via OTP, sent by the Payment Gateway.</li>
</ul>
<h4>How the Company may use your Personal Information</h4>
<ul>
    <li>The Company will only use your personal information/data in a fair and reasonable manner, and where we have a lawful reason to do so. We may use/process your Personal Information for the following purposes:
        <ul>
            <li>Protecting our Members/Visitors and providing them with customer support</li>
            <li>We also use this data for providing, testing, improving, or recommending the Services.</li>
            <li>Sending Members information about our products and services </li>
            <li>Conducting market research and surveys with the aim of improving our products and services.</li>
            <li>We use the data from the device you access our Website to identify the login information of multiple users from the same device.</li>
            <li>We use this data to enable you to make payments for our Services. We use a third-party service provider to manage payment processing. This service provider is not permitted to store, retain, or use information you provide except for the sole purpose of payment processing on our behalf.</li>
            <li>We use this data for processing your requests, enquiries and complaints, customer services and related activities.</li>
            <li>We use this data to communicate about existing or new offers, content, advertisements, surveys, key policies or other administrative information.</li>
            <li>We also use this data to provide you with informative features and services that are developed by us from time to time.</li>
            <li>Establishing, exercising or defending legal rights in connection with legal proceedings (including any prospective legal proceedings) and seeking professional or legal advice in relation to such legal proceedings.</li>
        </ul>
    </li>
    <li>Company may disclose your personal information, without notice, if required to do so by law or in the good faith belief that such action is necessary to: (a) conform to the edicts of the law or comply with legal process served on Company or the site; (b) protect and defend the rights or property of Company; and/or (c) act under exigent circumstances to protect the personal safety of users of Company, or the public.</li>
</ul>
<h4>Published Content</h4>
<ul>
    <li>Members may choose to submit/publish/post their testimonials, success stories, comments, messages, blogs, scribbles etc. on the public interface of the Website/Website/App ("<strong>Published Content</strong>")</li>
    <li>Such above-mentioned Published Content shall be displayed on our Website/App so long as you consent. The Company/Website/App agrees to take down such Published Content on receipt of written request by the relevant Member</li>
    <li>However, we are not responsible for any actions taken by third-parties with respect to such Published Content</li>
</ul>
<h4>Consent</h4>
<ul>
    <li>The Website/App relies on your consent in order to collect, use or process your Personal Information in certain situations.</li>
    <li>If Website/App requires your consent to collect and process certain Personal Information, as per the requirements under the applicable data protection laws, your consent is sought at the time of collection of such Personal Information and its processing will only be performed once consent is received.</li>
</ul>
<h4>Data Storage and Security Practices and Procedures</h4>
<ul>
    <li>Your personal data will primarily be stored in electronic form. We may engage third parties to collect, store, process your personal data under confidentiality agreements and full compliance with applicable laws. In the event, you have any telephonic interactions with our customer representatives; the call data is recorded and stored on the Amazon WebServices (AWS)/Google Cloud Data center residing Indian territories for training and quality purposes.</li>
    <li>We implement industry-standard technical and organizational measures by using a variety of security technologies and procedures to help protect your data from unauthorized access, use, loss, destruction or disclosure. When we collect particularly sensitive data (such as a credit card number or your geo-location), it is encrypted using industry-standard cryptographic techniques including but not limited to PBKDF2, AES256, TLS1.2 & SHA256.</li>
    <li>The Company/Website/App fully complies with all Indian Laws applicable, including without limitation, Rule 8 under Section 43A of the Information Technology Act, 2000.</li>
    <li>We have also ensured that reasonable security practices and procedures are in place as required under Rule 8 under Section 43A of the Information Technology Act, 2000.</li>
    <li>The Company/Website/App may disclose all or part of your personal details in response to a lawful request from the law enforcement authorities or in a case of bona fide requirement to prevent an imminent breach of the law</li>
</ul>
<h4>Security Precautions</h4>
<ul>
    <li>We aim to protect your personal information through a system of organizational and technical security measures and have implemented appropriate internal control measures designed to protect the security of any personal information we collect/ process.</li>
    <li>We strive to take appropriate security measures to protect against unauthorized access to or alteration of your personal information. Unfortunately, no data transmission over the Internet or any wireless network can be guaranteed to be 100% secure. As a result, while we strive to protect your personal information, you acknowledge that: (a) there are security and privacy limitations inherent to the Internet which are beyond our control; and (b) security, integrity, and privacy of any and all information and data exchanged between you and us through this Site cannot be guaranteed.</li>
</ul>
<h4>Disclosure and sharing of Personal Information</h4>
<ul>
    <li>The Website/App/the Company does not provide any Personal Information to the advertiser when you interact or view a targeted advertisement. However, by interacting with or viewing an ad you are consenting to the possibility that the advertiser may make the assumption that you meet the targeting criteria used to display the ad.</li>
    <li>
        The Website/App/the Company does not rent, sell, or share personal information about you with other people (save with your consent) or non-affiliated companies except to provide products or services you've requested, when we have your permission, or under the following circumstances:
        <ul>
            <li>If Website/App/the Company believes it is necessary to share information in order to investigate, prevent, or take action regarding illegal activities, suspected fraud, situations involving potential threats to the physical safety of any person, violations of Website/App’s terms of use, or as otherwise required by law</li>
            <li>We may transfer information about you if the Website/App is acquired by or merged with another company. In this event, Website/App will notify you before information about you is transferred and becomes subject to a different privacy policy</li>
            <li>In the event Website/App/the Company needs to respond to subpoenas, court orders, or legal process, or to establish or exercise our legal rights or defend against legal claims;</li>
            <li>Website/App/the Company provides the information to trusted partners who work on behalf of or with Website/App under confidentiality agreements. </li>
        </ul>
    </li>
</ul>
<h4>Retention of Personal Information</h4>
<ul>
    <li>The Company will retain the information we collect from Members/Visitors  under the following circumstances
        <ul>
            <li>For as long as the Member retains an active account with the Website/App or subscription to our Services</li>
            <li>for the sake of enforcing agreements</li>
            <li>for performing audits</li>
            <li>for resolving any form of disputes</li>
            <li>for establishing legal defences</li>
            <li>for pursuing legitimate businesses and to comply with the relevant applicable laws.</li>
        </ul>
    </li>
    <li>However, your data will be deleted if you request for the same. This deletion does not include the non identifiable meta  data derived from your data.</li>
    <li>Your Personal Information will not be retained by the Company/Website/App any longer than it is necessary for the purposes for which the Personal Information is collected and/or in accordance with regulatory, contractual or statutory obligations as may be applicable.</li>
    <li>At the expiry of such periods, your Personal Information will be deleted or archived in compliance with applicable laws.</li>
</ul>
<h4>Third Party Content</h4>
<ul>
    <li>Members/Visitors must keep in mind that the Website/App may sometimes contain links to other websites that are not governed by this Privacy Policy.</li>
    <li>Members/Visitors are advised to visit such third party websites and review their privacy policy for more information</li>
    <li>The Website/App/Company makes no representations or warranties regarding how your information may be stored or used by third-party websites.</li>
</ul>
<h4>Changes of Privacy Policy</h4>
<ol type="A">
    <li>We reserve the right to amend/alter or change all or any of the terms of this Privacy Policy at any time without any prior notice.</li>
    <li>Any changes in the Privacy Policy shall come to effect from the date of publication of such update</li>
    <li>Any changes or modifications will be updated on the Privacy Policy page of the Website.</li>
</ol>
<h4>Access, Correction & Deletion</h4>
<ul>
    <li>Members may request access, correction, modification and deletion of the data via email on <a href="mailto:connect@nugenomics.in">connect@NuGenomics.in</a></li>
    <li>You may note that deletion of certain data or withdrawal of consent may lead to cancellation of your account or your access to our Services.</li>
    <li>Based on technical feasibility, we will provide you with access to all your personal and sensitive personal data that we maintain about you. However such access shall only be provided once Member requesting such access is verified by us</li>
</ul>
</div>
</div>
</section>

<?php get_footer();?>