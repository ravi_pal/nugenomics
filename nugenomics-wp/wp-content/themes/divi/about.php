<?php /*Template Name: About Us*/

get_header();?>

<style>
    body.page.page-id-194 .profile-container{ display:none}
</style>

<section class="content-section">
        <div class="block-title text-center mb-40"><?php the_title();?></div>
        <div class="container">
    <div class="content-container">
<?php the_content();?>
</div>
<div class="profile-container">
    
<div class="block-title text-center mb-20">Our Team</div>
                <div class="row">
<?php 
        $the_query = new WP_Query(array('post_type' => 'employee', 'orderby'=>'ID', 'order' => 'ASC', 'paged' => get_query_var('paged') ? get_query_var('paged') : 1 ));
        if($the_query->have_posts()): 
        while ($the_query -> have_posts()) : $the_query -> the_post();
        $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );  
//$getsecondimage=$dynamic_featured_image->get_featured_images([$post->ID]);
//$getimagesecondurl=$getsecondimage[0]['full'];
          ?>        
                    <div class="col-md-4">
                        <div class="profile">
                            <div class="profile-head d-flex align-items-center">
                                <div class="avatar">
                                    <img src="<?php if($url!=''){echo $url;}else{echo "/wp-content/uploads/2021/04/noavatar.jpg";} ?>" alt="<?php the_title();?>">
                                </div>
                                <div class="profile-desc">
                                    <ul>
<?php the_field('skiils');?>
                                    </ul>
                                </div>
                            </div>
                            <div class="profile-name">
                                <strong><?php the_title();?></strong>
<span class="designation"><?php the_content();?></span>
                            </div>
                        </div>
                    </div>
            <?php  endwhile; ?>
                                  <?php endif; 
          wp_reset_query();?>

                    <!--div class="col-md-4">
                        <div class="profile profile-bg-1">
                            <div class="profile-head d-flex align-items-center">
                                <div class="avatar">
                                    <img src="<?php //bloginfo('template_directory'); ?>/images/profile1.jpg" alt="">
                                </div>
                                <div class="profile-desc">
                                    <ul>

                                    </ul>
                                </div>
                            </div>
                            <div class="profile-name">
                            <strong>Dr. Shanthi Lakshmi Duraimani</strong>
<span class="designation">Nutri-Genomist</span>
                            </div>
                        </div>
                    </div-->
                    <!--div class="col-md-4">
                        <div class="profile">
                            <div class="profile-head d-flex align-items-center">
                                <div class="avatar">
                                    <img src="<?php //bloginfo('template_directory'); ?>/images/noavatar.jpg" alt="">
                                </div>
                                <div class="profile-desc">
                                    <ul>
                                        <li>CSIR Research Fellow
                                            
                                            </li>
                                            <li>MBA from 
                                                XLRI Jamshedpur
                                                
                                                
                                                
                                                </li>
                                                <li>6+ years of experience 
                                                    across domains
                                                    </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="profile-name">
                            <strong>Rahul Ranganathan</strong>

<span class="designation">CoFounder – Molecular Biologist</span>
                            </div>
                        </div>
                    </div-->
                </div>
           </div>
</div>
</section>
<div class="modal fade custom-modal" id="frmModal" tabindex="-1" role="dialog" aria-labelledby="frmModalLabel" aria-hidden="true">
   <div class="modal-dialog" role="document">
     <div class="modal-content">
       <div class="modal-header">
         <h5 class="modal-title" id="frmModalLabel">Get in Touch</h5>
         <button type="button" class="close" data-dismiss="modal" aria-label="Close">
           <span aria-hidden="true">&times;</span>
         </button>
       </div>
       <div class="modal-body">
         <div class="row">
            <div class="col-lg-10 offset-lg-1">
               <div class="frm-holder mt-4">
<?php echo do_shortcode('[contact-form-7 id="218" title="Contact form 1"]');?>
               </div>
            </div>
         </div>
       </div>
     </div>
   </div>
 </div>
<?php get_footer();?>