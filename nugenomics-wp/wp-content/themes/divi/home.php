<?php /*Template Name:Home*/

get_header();?>

<section class="herosection">
         <div id="heroslider">
            <div class="slide">
               <div class="slide-img">
                  <img src="<?php bloginfo('template_directory'); ?>/images/banner1.jpg" width="100%" alt="">
               </div>
            </div>
            <div class="slide">
               <div class="slide-img">
               <img src="<?php bloginfo('template_directory'); ?>/images/banner2.jpg" width="100%" alt="">
               </div>
            </div>
            <div class="slide">
               <div class="slide-img">
               <img src="<?php bloginfo('template_directory'); ?>/images/banner3.jpg" width="100%" alt="">
               </div>
            </div>
            <div class="slide">
               <div class="slide-img">
               <img src="<?php bloginfo('template_directory'); ?>/images/banner4.jpg" width="100%" alt="">
               </div>
            </div>
         </div>
         <div class="discover-more-arrow text-center">
            Discover the product
            <i class="fa fa-chevron-down  bounce icon"></i>
         </div>
      </section>
      <section class="content-section">
         <div class="site-container">
            <div class="block-title mb-50">Become the best version of yourself</div>
            <div id="slider-1">
               <div class="slide-item">
                  <div class="featured-block text-center">
                     <div class="fblock-img mb-30">
                        <img src="<?php bloginfo('template_directory'); ?>/images/vector-1.png" alt="">
                     </div>
                     <div class="fblock-title block-title fsize-sm mb-15">
                        What do the genes say?
                     </div>
                     <div class="pwrap">
                        <p>Good heart health, poor digestion, great immunity - what have you inherited?</p>
                     </div>
                  </div>
               </div>
               <div class="slide-item">
                  <div class="featured-block text-center">
                     <div class="fblock-img mb-30">
                        <img src="<?php bloginfo('template_directory'); ?>/images/vector-2.png" alt="">
                     </div>
                     <div class="fblock-title block-title fsize-sm mb-15">
                        What do the Blood Biomarkers say? 
                     </div>
                     <div class="pwrap">
                        <p>Can’t make sense of your blood reports without a doctor? Let us take care of it! </p>
                     </div>
                  </div>
               </div>
               <div class="slide-item">
                  <div class="featured-block text-center">
                     <div class="fblock-img mb-30">
                        <img src="<?php bloginfo('template_directory'); ?>/images/vector-3.png" alt="">
                     </div>
                     <div class="fblock-title block-title fsize-sm mb-15">
                        What does your lifestyle say? 
                     </div>
                     <div class="pwrap">
                        <p>No, you don’t have to give-up on ‘Fun Fridays’.. Good health & fun can go hand-in-hand </p>
                     </div>
                  </div>
               </div>
               <div class="slide-item">
                  <div class="featured-block text-center">
                     <div class="fblock-img mb-30">
                        <img src="<?php bloginfo('template_directory'); ?>/images/which-super.png" alt="">
                     </div>
                     <div class="fblock-title block-title fsize-sm mb-15">
                        Which foods are SUPER for you?
                     </div>
                     <div class="pwrap">
                        <p>It’s not a myth, superfoods are SUPER for a reason!</p>
                     </div>
                  </div>
               </div>
               <div class="slide-item">
                  <div class="featured-block text-center">
                     <div class="fblock-img mb-30">
                        <img src="<?php bloginfo('template_directory'); ?>/images/starve.png" alt="">
                     </div>
                     <div class="fblock-title block-title fsize-sm mb-15">
                        Don’t worry, you wouldn’t have to starve!
                     </div>
                     <div class="pwrap">
                        <p>You will fall in love with eating the right food for your body </p>
                     </div>
                  </div>
               </div>
               <div class="slide-item">
                  <div class="featured-block text-center">
                     <div class="fblock-img mb-30">
                        <img src="<?php bloginfo('template_directory'); ?>/images/support.png" alt="">
                     </div>
                     <div class="fblock-title block-title fsize-sm mb-15">
                        We got your back! 
                     </div>
                     <div class="pwrap">
                        <p>Oh come on! When have you gotten anything great done without some pushing? We’re here! </p>
                     </div>
                  </div>
               </div>

            </div>
            <div class="text-center mt-50">
               <a href="/product/dna-based-holistic-wellness-solution/" class="button">Shop Now</a>
            </div>
         </div>
      </section>
      <section class="content-section lightbg pos-rel">
         
         <div class="fixed-vector-1">
            <img src="<?php bloginfo('template_directory'); ?>/images/vector-13.png" alt="">
         </div>
         
         <div class="fixed-vector-2">
            <img src="<?php bloginfo('template_directory'); ?>/images/vector-14.png" alt="">
         </div>
         <div class="site-container">
            <div class="block-title clr-dark mb-50">The 
               NuGenomic 
               Approach 
            </div>
            <div id="slider-2" class="dots-theme-blue dots-centered">
               
               <div class="slide-item">
                     <div class="white-card">
                        <div class="white-card-body">
                           <div class="block-title fsize-md mb-20">
                              Input 
                           </div>
                           <div class="input-list">
                              <ul>
                                 <li><a href=""><img src="<?php bloginfo('template_directory'); ?>/images/icons/icon-1.png" class="icon" />Genomics Data</a></li>
                                 <li><a href=""><img src="<?php bloginfo('template_directory'); ?>/images/icons/icon-2.png" class="icon" />Blood Report</a></li>
                                 <li><a href=""><img src="<?php bloginfo('template_directory'); ?>/images/icons/icon-3.png" class="icon" />Lifestyle</a></li>
                                 <li><a href=""><img src="<?php bloginfo('template_directory'); ?>/images/icons/icon-4.png" class="icon" />Medical History</a></li>
                              </ul>
                           </div>
                        </div>
                     </div>
               </div>
               <div class="slide-item">
                     <div class="white-card">
                        <div class="white-card-body">
                           <div class="block-title fsize-md mb-20">
                              Processing
                           </div>
                           <div class="pwrap clr-dark">
                              <p>NuGenomics AI Powered Predictive 
                                 Health Analytics
                              </p>
                           </div>
                           <div class="card-img">
                              <img src="<?php bloginfo('template_directory'); ?>/images/vector-4.png" alt="">
                           </div>
                           <div class="pwrap text-center">
                              <p>Creates a Comprehensive Picture 
                                 of Current and Future health
                              </p>
                           </div>
                        </div>
                     </div>
               </div>
               <div class="slide-item">
                     <div class="white-card">
                        <div class="white-card-body">
                           <div class="block-title fsize-md mb-20">
                              Output
                           </div>
                           <div class="pwrap clr-dark">
                              <p>NuGenomics AI Powered Predictive 
                                 Recommendation System
                              </p>
                           </div>
                           <div class="card-img">
                              <img src="<?php bloginfo('template_directory'); ?>/images/vector-5.png" alt="">
                           </div>
                           <div class="pwrap text-center">
                              <p>DNA based food and lifestyle 
                                 recommendations to optimize health
                              </p>
                           </div>
                        </div>
                  </div>
               </div>
            </div>
            <div class="text-center mt-50">
               <a href="/product/dna-based-holistic-wellness-solution/" class="button">Shop Now</a>
            </div>
         </div>
      </section>
      <section class="content-section result-expectation-section pos-rel">
         
         <div class="fixed-vector-3">
            <img src="<?php bloginfo('template_directory'); ?>/images/vector-15.png" alt="">
         </div>
         <div class="site-container">
            <div class="row">
               <div class="col-md-6 col-lg-4 nth-child-1">
                  <div class="block-title clr-dark mb-50">What Results to Expect?</div>
               </div>
               <div class="col-md-6 col-lg-4 nth-child-2">
                  <div class="featured-block text-center mb-50">
                     <div class="fblock-title block-title fsize-sm mb-20 clr-pink">
                        Month 1
                     </div>
                     <div class="pwrap">
                        <p>Are you feeling more active, light on your feet lately? Kudos! Change has begun!</p>
                     </div>
                  </div>
               </div>
               <div class="col-md-12 nth-child-3">
                  <div class="block-img mb-70 text-center">
                     <img src="<?php bloginfo('template_directory'); ?>/images/vector-6.png" alt="">
                  </div>
               </div>
               <div class="col-md-6 nth-child-4">
                  <div class="featured-block mb-50 text-center">
                     <div class="fblock-title block-title fsize-sm mb-20 clr-pink">
                        Month 3 and beyond
                     </div>
                     <div class="pwrap">
                        <p>YES! You’re finally changing for the better! Doesn’t it feel awesome?</p>
                     </div>
                  </div>
               </div>
               <div class="col-md-6 nth-child-5">
                  <div class="featured-block mb-50 text-center">
                     <div class="fblock-title block-title fsize-sm mb-20 clr-pink">
                        Month 6 & beyond
                     </div>
                     <div class="pwrap">
                        <p>WOAH! Is this what it feels like to be a better version of yourself?</p>
                     </div>
                  </div>
               </div>
            </div>
            <div class="text-center mt-50">
               <a href="/product/dna-based-holistic-wellness-solution/" class="button">Shop Now</a>
            </div>
         </div>
         <div class="fixed-vector-4">
            <img src="<?php bloginfo('template_directory'); ?>/images/vector-16.png" alt="">
         </div>
      </section>
      <section class="content-section lightbg">
         <div class="site-container">
            <div class="block-title clr-dark mb-20 text-center">How it works?</div>
            <div class="steps-container d-flex flex-wrap dots-theme-blue dots-centered">
               <div class="step-block-outer">
                  <div class="step-block text-center">
                     <div class="step-count block-title fsize-sm">01</div>
                     <div class="step-img mb-30"><img src="<?php bloginfo('template_directory'); ?>/images/vector-7.png" width="100%" alt=""></div>
                     <div class="step-meta">
                        <div class="block-title fsize-sm mb-15">Sign-up</div>
                        <div class="pwrap">
                           <p>Let’s get you on-board!</p>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="step-block-outer">
                  <div class="step-block text-center">
                     <div class="step-count block-title fsize-sm">02</div>
                     <div class="step-img mb-30"><img src="<?php bloginfo('template_directory'); ?>/images/vector-8.png" width="100%" alt=""></div>
                     <div class="step-meta">
                        <div class="block-title fsize-sm mb-15">Sample Collection</div>
                        <div class="pwrap">
                           <p>Just a PINCH and you’re done</p>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="step-block-outer">
                  <div class="step-block text-center">
                     <div class="step-count block-title fsize-sm">03</div>
                     <div class="step-img mb-30"><img src="<?php bloginfo('template_directory'); ?>/images/vector-9.png" width="100%" alt=""></div>
                     <div class="step-meta">
                        <div class="block-title fsize-sm mb-15">Lifestyle Analysis</div>
                        <div class="pwrap">
                           <p>Here’s the real deal with your lifestyle - and here’s how we fix it</p>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="step-block-outer">
                  <div class="step-block text-center">
                     <div class="step-count block-title fsize-sm">04</div>
                     <div class="step-img mb-30"><img src="<?php bloginfo('template_directory'); ?>/images/vector-10.png" width="100%" alt=""></div>
                     <div class="step-meta">
                        <div class="block-title fsize-sm mb-15">Comprehensive
                           Health Profile
                        </div>
                        <div class="pwrap">
                           <p>Genetic, blood & lifestyle analysis - RELAX, even a 5-year old can understand it</p>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="step-block-outer">
                  <div class="step-block text-center">
                     <div class="step-count block-title fsize-sm">05</div>
                     <div class="step-img mb-30"><img src="<?php bloginfo('template_directory'); ?>/images/vector-11.png" width="100%" alt=""></div>
                     <div class="step-meta">
                        <div class="block-title fsize-sm mb-15">Nutrigenomics
                           Recommendations
                        </div>
                        <div class="pwrap">
                           <p>Here it is - your personalized food & lifestyle plan! It’s game time!</p>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="step-block-outer">
                  <div class="step-block text-center">
                     <div class="step-count block-title fsize-sm">06</div>
                     <div class="step-img mb-30"><img src="<?php bloginfo('template_directory'); ?>/images/vector-12.png" width="100%" alt=""></div>
                     <div class="step-meta">
                        <div class="block-title fsize-sm mb-15">Getting Started</div>
                        <div class="pwrap">
                           <p>You start, we clap. You stop, we push! Go buddy, we got you!</p>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="text-center mt-50">
               <a href="/product/dna-based-holistic-wellness-solution/" class="button">Shop Now</a>
            </div>
         </div>
      </section>
      <section class="content-section pb-0">
         <div class="site-container">
            <div class="row">
               <div class="col-lg-5">
               <div class="block-title fsize-md mb-4">We Pinky Promise...</div>
               <div class="pwrap mb-4">
                  <p>That when it comes to providing our service, privacy, thorough research and expert approved science is our priority. To know more about our promises,</p>
               </div>
               <a href="/pinky-promise" class="button mb-5">Click Here</a>
            </div>
            <div class="col-lg-6 offset-lg-1">
               <div style="line-height:0;">
               <img src="<?php bloginfo('template_directory'); ?>/images/pinky-promise.svg" alt="">
               </div>
            </div>
            </div>
         </div>
      </section>
      <section class="content-section lightbg">
         <div class="site-container">
            <div class="articles-container">
               <div class="block-title text-center fsize-md mb-50"><a style="color:inherit;" href="https://www.instagram.com/nugenomics.in/" target="_blank"><span class="fa fa-instagram icon"></span>Follow us on Instagram</a></div>
               <?php echo do_shortcode('[enjoyinstagram_mb]');?>
            </div>
         </div>
      </section>
<div class="modal fade custom-modal" id="frmModal" tabindex="-1" role="dialog" aria-labelledby="frmModalLabel" aria-hidden="true">
   <div class="modal-dialog" role="document">
     <div class="modal-content">
       <div class="modal-header">
         <h5 class="modal-title" id="frmModalLabel">Get in Touch</h5>
         <button type="button" class="close" data-dismiss="modal" aria-label="Close">
           <span aria-hidden="true">&times;</span>
         </button>
       </div>
       <div class="modal-body">
         <div class="row">
            <div class="col-lg-10 offset-lg-1">
               <div class="frm-holder mt-4">
<?php echo do_shortcode('[contact-form-7 id="218" title="Contact form 1"]');?>
               </div>
            </div>
         </div>
       </div>
     </div>
   </div>
 </div>
<?php get_footer();?>