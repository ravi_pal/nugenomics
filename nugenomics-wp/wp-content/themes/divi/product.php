<?php /*Template Name:Shop*/
get_header();?>

<link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/css/et_core.css" />

<div id="main-content">
          <div id="et-boc" class="et-boc">
             <div class="et-l et-l--post">
                <div class="et_builder_inner_content et_pb_gutters3">
                   <div class="et_pb_section et_pb_section_0 et_pb_with_background et_section_regular">
                      <div class="et_pb_row et_pb_row_0">
                         <div class="et_pb_column et_pb_column_4_4 et_pb_column_0  et_pb_css_mix_blend_mode_passthrough et-last-child">
                            <div class="et_pb_module et_pb_text et_pb_text_0  et_pb_text_align_center et_pb_bg_layout_light">
                               <div class="et_pb_text_inner">
                                  <h3>The finest spices and herbs</h3>
                                  <h1>Our <?php the_title();?></h1>
                               </div>
                            </div>
                         </div>
                      </div>
                      <div class="et_pb_row et_pb_row_1 et_pb_equal_columns et_pb_gutters2 et_pb_row_4col">
                        <?php 
                        $the_query = new WP_Query(array('post_type' => 'product','order' => 'DESC','posts_per_page' => 10, 'orderby'=>'ID', 'order' => 'ASC', 'paged' => get_query_var('paged') ? get_query_var('paged') : 1 ));
                        if(have_posts()): 
                        while ($the_query -> have_posts()) : $the_query -> the_post();
                        $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );  
                          ?>
                         <div class="et_pb_column et_pb_column_1_4 et_pb_column_1  et_pb_css_mix_blend_mode_passthrough">
                            <div class="et_pb_module et_pb_image et_pb_image_0">
                               <span class="et_pb_image_wrap "><img loading="lazy" src="<?php echo $url;?>" alt="" title="spice-shop-63" height="auto" width="auto" srcset="<?php echo $url;?> 400w, <?php echo $url;?> 289w" sizes="(max-width: 400px) 100vw, 400px" class="wp-image-103"></span>
                            </div>
                         </div>
                         <div class="et_pb_column et_pb_column_1_4 et_pb_column_2  et_pb_css_mix_blend_mode_passthrough">
                            <div class="et_pb_module et_pb_image et_pb_image_1">
                               <span class="et_pb_image_wrap "><img loading="lazy" src="<?php bloginfo('template_directory'); ?>/images/spice-shop-43.png" alt="" title="spice-shop-43" width="auto" height="auto"></span>
                            </div>
                            <div class="et_pb_module et_pb_text et_pb_text_1  et_pb_text_align_left et_pb_bg_layout_light">
                               <div class="et_pb_text_inner">
                                  <h3><?php the_title();?></h3>
                                  <p><?php 
                                    $fullcontent=get_the_content();
                                    echo wp_trim_words($fullcontent,10,'');?></p>
                               </div>
                            </div>
                            <div class="et_pb_button_module_wrapper et_pb_button_0_wrapper  et_pb_module ">
                               <a class="et_pb_button et_pb_button_0 et_pb_bg_layout_light" href="<?php the_permalink();?>">Shop</a>
                            </div>
                         </div>
                         <?php  endwhile; ?>
                         <?php endif; 
 wp_reset_query();?>
                      </div>
                   </div>
                   
                   <div class="et_pb_section et_pb_section_2 et_pb_with_background et_section_regular">
                      <div class="et_pb_row et_pb_row_3">
                         <div class="et_pb_column et_pb_column_4_4 et_pb_column_6  et_pb_css_mix_blend_mode_passthrough et-last-child">
                            <div class="et_pb_module et_pb_text et_pb_text_3 et_pb_text_align_center et_pb_bg_layout_dark et_had_animation">
                               <div class="et_pb_text_inner">
                                  <h3>Join Today</h3>
                               </div>
                            </div>
                            <div class="et_pb_module et_pb_text et_pb_text_4 et_pb_text_align_center et_pb_bg_layout_dark et_had_animation">
                               <div class="et_pb_text_inner">
                                  <h2>Spice Club</h2>
                               </div>
                            </div>
                            <div class="et_pb_module et_pb_image et_pb_image_4">
                               <span class="et_pb_image_wrap "><img loading="lazy" src="<?php bloginfo('template_directory'); ?>/images/spice-shop-64.png" alt="" title="spice-shop-64" width="auto" height="auto"></span>
                            </div>
                         </div>
                      </div>
                      <div class="et_pb_row et_pb_row_4">
                         <div class="et_pb_column et_pb_column_4_4 et_pb_column_7  et_pb_css_mix_blend_mode_passthrough et-last-child">
                            <div class="et_pb_button_module_wrapper et_pb_button_2_wrapper et_pb_button_alignment_center et_pb_module ">
                               <a class="et_pb_button et_pb_button_2 et_pb_bg_layout_light" href="/shop">Become a Spice Club Member</a>
                            </div>
                            <div class="et_pb_module et_pb_text et_pb_text_5  et_pb_text_align_center et_pb_bg_layout_light">
                               <div class="et_pb_text_inner">
                                  <p>&nbsp;Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est</p>
                               </div>
                            </div>
                            <div class="et_pb_module et_pb_text et_pb_text_6  et_pb_text_align_center et_pb_bg_layout_light">
                               <div class="et_pb_text_inner">
                                  <p>only $14 / mo</p>
                               </div>
                            </div>
                         </div>
                      </div>
                   </div>
                   <div class="et_pb_section et_pb_section_3 et_pb_with_background et_section_regular">
                      <div class="et_pb_row et_pb_row_5">
                         <div class="et_pb_column et_pb_column_1_2 et_pb_column_8  et_pb_css_mix_blend_mode_passthrough">
                            <div class="et_pb_module et_pb_text et_pb_text_7  et_pb_text_align_left et_pb_bg_layout_light">
                               <div class="et_pb_text_inner">
                                  <h3>Address</h3>
                                  <p><a href="#">5678 Extra Rd. #123 San Francisco, CA 96120.</a></p>
                               </div>
                            </div>
                            <div class="et_pb_module et_pb_text et_pb_text_8  et_pb_text_align_left et_pb_bg_layout_light">
                               <div class="et_pb_text_inner">
                                  <h3>Phone Number</h3>
                                  <p><a href="#">(255) 352-6258</a></p>
                               </div>
                            </div>
                            <div class="et_pb_module et_pb_text et_pb_text_9  et_pb_text_align_left et_pb_bg_layout_light">
                               <div class="et_pb_text_inner">
                                  <h3>Open EveryDay</h3>
                                  <p>8AM – 5PM</p>
                               </div>
                            </div>
                            <div class="et_pb_button_module_wrapper et_pb_button_3_wrapper  et_pb_module ">
                               <a class="et_pb_button et_pb_button_3 et_pb_bg_layout_light" href="">Visit The Shop</a>
                            </div>
                         </div>
                         <div class="et_pb_column et_pb_column_1_2 et_pb_column_9  et_pb_css_mix_blend_mode_passthrough et-last-child et_pb_column_empty"></div>
                      </div>
                   </div>
                </div>
             </div>
          </div>
 </div>
 
 

<?php get_footer();?>
