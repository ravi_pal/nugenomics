<footer id="site-footer">
         <div class="site-container">
            <div class="block-title fsize-md clr-white mb-50">Contact Us</div>
            <div class="row">
               <div class="col-md-4 mbs">
                  <div class="contact-block mb-40">
                  <a href="#" target="_blank" data-toggle="modal" data-target="#frmModal" style="margin-top: 0px !important;" class="button mt-30">Get in touch</a>
                  </div>
               </div>

               <div class="col-md-4">
                  <div class="contact-block mb-40">
                     <div class="contact-title mb-15">Address</div>
                     <div class="contact-para">D-29A, 
                        Central Market, Lajpat Nagar-2, 
                        New Delhi
                     </div>
                     <a href="#" class="button mt-30">Get the direction</a>
                  </div>
               </div>
               <div class="col-md-4">
                  <div class="contact-block mb-40">
                     <div class="contact-title mb-15">Phone NUmber</div>
                     <div class="contact-para">+91 9176655912</div>
                  </div>
                  <!-- <div class="contact-block mb-40">
                     <div class="contact-para"><a href="https://wa.me/message/DPGHO2JNRWEXO1" target="_blank"><img class="icon" src="<?php bloginfo('template_directory'); ?>/images/icons/whatsapp-icon.png" alt="" />+91 8951518761</a></div>
                  </div> -->
               </div>
               <div class="col-md-4">
                  <div class="contact-block mb-40">
                     <div class="contact-title mb-15">Email Us</div>
                     <div class="contact-para">
                        <a href="mailto:support@nugenomics.in">support@nugenomics.in</a>
                  <a href="#" target="_blank" data-toggle="modal" data-target="#frmModal" class="button mt-30 ds">Get in touch</a>
                     </div>
                  </div>
               </div>
            </div>
            <div class="row">
               <div class="col-lg-12">
                  <div class="footer-bottom clearfix">
                     <div class="social-network">
                        <a class="facebook" href="https://www.facebook.com" target="_blank"><span class="fa fa-facebook"></span></a>
                        <a class="twitter" href="https://twitter.com" target="_blank"><span class="fa fa-twitter"></span></a>
                        <a class="linkdin" href="https://www.youtube.com/" target="_blank"><span class="fa fa-youtube-play"></span></a>
                     </div>
                     <div class="footer-left clearfix">
                        <div class="flinks">
                           <ul>
                              <li>
                                 <a href="/terms-and-conditions/">Terms & Conditions</a>
                              </li>
                              <li>
                                 <a href="/privacy-policy">Privacy Policy</a>
                              </li>
                              <li>
                                 <a href="/cancellation-and-refund-policy">Cancellation & Refund Policy</a>
                              </li>
                           </ul>
                        </div>
                        <div class="copyright">© 2021 nugenomics.com. All rights reserved.<br> <a href="https://answergenomics.in/" target="_blank">Powered by Answer Genomics</a></div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </footer>

              <!-- Modal -->
<div class="modal fade custom-modal" id="keepInTouchModal" tabindex="-1" role="dialog" aria-labelledby="keepInTouchLabel" aria-hidden="true">
   <div class="modal-dialog" role="document">
     <div class="modal-content">
       <div class="modal-header">
         <!-- <h5 class="modal-title" id="keepInTouchLabel"></h5> -->
         <button type="button" class="close" data-dismiss="modal" aria-label="Close">
           <span aria-hidden="true">&times;</span>
         </button>
       </div>
       <div class="modal-body">
         <div class="pwrap text-center clr-light mb-2">
            <p>"Want to know when we've got something Nu for You?"</p>
         </div>
         <div class="block-title fsize-md text-center mb-3">Let's keep in touch</div>
         <div class="block-img mb-4 text-center">
            <img src="<?php bloginfo('template_directory'); ?>/images/keep-in-touch.png" width="240" alt="">
         </div>
         <div class="row">
            <div class="col-lg-10 offset-lg-1">
               <div class="frm-holder">
<?php echo do_shortcode('[contact-form-7 id="418" title="Main Popup"]');?>
               </div>
            </div>
         </div>
       </div>
     </div>
   </div>
 </div>

      <script src="<?php bloginfo('template_directory'); ?>/js/popper.min.js"></script>
      <script src="<?php bloginfo('template_directory'); ?>/js/bootstrap.min.js"></script>
      <script src="<?php bloginfo('template_directory'); ?>/js/slick.min.js" type="text/javascript"></script>
	<!-- custom cursor js-->
	<script src="<?php bloginfo('template_directory'); ?>/js/kursor.min.js" type="text/javascript"></script>
	<!-- custom js -->
	<script src="<?php bloginfo('template_directory'); ?>/js/nue-custom.js" type="text/javascript"></script>

   <script type="text/javascript">
// setTimeout(popupmethod, 10000);
// function popupmethod()
// {
//         if(sessionStorage.getItem('popState') != 'shown'){
//             jQuery('#keepInTouchModal').modal('show');
//             sessionStorage.setItem('popState','shown')
//         }
// }
if(sessionStorage.getItem('popState') != 'shown'){
            setTimeout(function(){
               jQuery('#keepInTouchModal').modal('show');
               sessionStorage.setItem('popState','shown')
            }, 15000)
        }
    </script>

	<?php wp_footer(); ?>
</body>
</html>
