<?php /*Template Name: Cancellation and Refund Policy*/

get_header();?>

<section class="content-section">
        <div class="container">
        <div class="block-title text-center mb-40">Cancellation and Refund Policy </div>
    <div class="content-container">
    <ol>
        <li>You may cancel your purchase within 24 hours of buying the test or before the phlebotomist collects your blood sample (whichever is earlier). </li>

        <li>To claim a refund, kindly support it with related documents/ reports. </li>

        <li>In normal cases, any blood report comparisons beyond 7 days will not be considered. </li>

        <li>In the case of chronic diseases, an exception can be made up to 15 days, only after approval from our Quality team. </li>

        <li>In the case of Thyroid, TSH values can vary due to various factors like time of sample collection and fasting status. Only significant variations in TSH levels will be considered. </li>

        <li>Any variation in Lipid profile will not be considered. </li>

        <li>In the case of any discrepancy in blood test results, a free re-sample will be collected and send to 2 partner labs. 1 sample will be sent to our partner lab and another to a standard lab. If a variation is found in the results, we will refund the entire test amount. </li>

        <li>In case we are not able to collect the blood sample or your saliva sample, full amount deducted will be refunded. </li>

        <li>Refund requests will be processed within 30 working days of receiving the request.</li> 

        <li>Our quality team reserves the right to make the final decision. </li>

        <li>For any queries regarding cancellation please contact support@nugenomics.in </li>
    </ol>
</div>
</div>
</section>
<?php get_footer();?>