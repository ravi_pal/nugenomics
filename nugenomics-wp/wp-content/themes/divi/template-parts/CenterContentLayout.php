 <section class="section-four pt-100 posrel">
         <div class="container">
            <div class="row">
               <div class="col-lg-8 offset-lg-2">
                  <div class="content-block text-center">
                     <div class="section-heading clr-dark mb-4 text-uppercase fsize-md"><?php the_sub_field('heading');?> <br><small class="font-primary"><?php the_sub_field('sub_heading');?></small></div>
                     <div class="para-holder mb-5">
                        <p><?php the_sub_field('description');?></p>
                     </div>
                     <div class="img-holder">
                        <img src="<?php the_sub_field('image');?>" width="400" alt="">
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="covid-vector vector-1">
            <img src="/wp-content/uploads/2021/06/covid-vector1.png" alt="">
         </div>
         <div class="covid-vector vector-2">
            <img src="/wp-content/uploads/2021/06/covid-vector2.png" alt="">
         </div>
         <div class="covid-vector vector-3">
            <img src="/wp-content/uploads/2021/06/covid-vector1.png" alt="">
         </div>
         <div class="covid-vector vector-4">
            <img src="/wp-content/uploads/2021/06/covid-vector1.png" alt="">
         </div>
      </section>