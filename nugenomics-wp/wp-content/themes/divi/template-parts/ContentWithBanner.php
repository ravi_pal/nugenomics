<section class="section-two posrel">
 <div class="banner-img banner-1 mb-3">
    <img src="<?php the_sub_field('mobile_banner');?>" width="100%" alt="">
 </div>
 <div class="container">
    <div class="row">
       <div class="col-lg-5 offset-lg-7">
          <div class="content-block">
             <h4 class="rotated hashtag clr-white"><?php the_sub_field('sub_heading');?></h4>
             <div class="divider-line mb-4"></div>
             <div class="section-heading text-uppercase mb-3 clr-white"><?php the_sub_field('heading');?>
             </div>
             <div class="para-holder clr-dark">
                <p><?php the_sub_field('description');?></p>
             </div>
          </div>
       </div>
    </div>
 </div>
 <div class="banner-img banner-2">
    <img src="<?php the_sub_field('desktop_banner');?>" width="100%" alt="">
 </div>
</section>