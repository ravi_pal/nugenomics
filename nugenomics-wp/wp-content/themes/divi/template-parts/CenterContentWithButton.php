 <?php $headingColour = get_sub_field('heading_colour');
 if($headingColour == '#ffffff'){
   $btnStyle = 'btn-style btn-style-white';
 }else{
   $btnStyle = 'btn-style';
 }?>
 <section class="section-seven pt-100 pb-100" style="background-color: <?php the_sub_field('background_colour');?>;">
         <div class="container">
            <div class="row">
               <div class="col-lg-8 offset-lg-2">
            <div class="content-block text-center">
               <div class="section-heading text-uppercase mb-4 fsize-md" style="color:<?php the_sub_field('heading_colour');?>"><?php the_sub_field('heading');?>
               </div>
               <?php if(get_sub_field('button_link')){?>
                    <a href="<?php the_sub_field('button_link');?>" target="_blank" class="<?php echo $btnStyle;?>"><?php the_sub_field('button_text');?></a> 
               <?php }else{?>
                  <a href="" target="_blank" data-toggle="modal" data-target="#frmModal" class="<?php echo $btnStyle;?>"><?php the_sub_field('button_text');?></a>
               <?php }?>
               
            </div>
            </div>
            </div>
         </div>
      </section>