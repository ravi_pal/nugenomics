 <div class="approach-content-block item-4 d-flex flex-wrap align-items-center">
                 <div class="approach-content-block-inn w-100">
                <div class="approach-content text-center">
                   <div class="block-title fsize-md text-center mb-5"><?php the_sub_field('heading');?></div>
                   <div class="block-img mb-5">
                      <img src="<?php the_sub_field('image');?>" width="300" alt="">
                   </div>
                   <div class="pwrap fsize-1 text-center clr-light">
                      <p><?php the_sub_field('description');?></p>
                   </div>
                </div>
                <?php if(have_rows('listing')): ?>
                <div class="key-feature key-feature-1 clr-light mt-5">
                   <ul>
                     <?php while (have_rows('listing')): the_row(); ?>
                      <li><?php the_sub_field('list');?></li>
                      <?php endwhile;?>
                   </ul>
                </div>
             <?php endif;?>
             </div>
            </div>