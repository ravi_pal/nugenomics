 <div class="approach-content-block item-5 d-flex flex-wrap align-items-center">
                <div class="approach-content-block-inn w-100">
                <div class="approach-content text-center">
                   <div class="block-title fsize-md text-center mb-2"><?php the_sub_field('heading');?></div>
                   <div class="block-img">
                      <img src="<?php the_sub_field('image');?>" width="340" alt="">
                   </div>
                   <div class="pwrap fsize-1 text-center clr-light">
                      <p><?php the_sub_field('description');?></p>
                   </div>
                </div>
             </div>
          </div>