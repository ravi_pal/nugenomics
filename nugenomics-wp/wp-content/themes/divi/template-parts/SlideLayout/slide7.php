<div class="approach-content-block dna-block item-7 d-flex flex-wrap align-items-center">
    <div class="approach-content-block-inn pos-rel w-100">
    <div class="approach-content clearfix">
       <div class="dna-img">
          <img src="<?php the_sub_field(image_1);?>" class="img-v1" alt="">
          <img src="<?php the_sub_field(image_2);?>" class="img-v2" alt="">
       </div>
       <?php if(have_rows('key_feature')):?>
       <div class="key-feature key-feature-2 clr-light">
          <ul>
          	<?php while(have_rows('key_feature')): the_row();?>
             <li><span class="connector"></span>
             <?php the_sub_field('feature');?>
             </li>
         <?php endwhile;?>
          </ul>
       </div>
   <?php endif;?>
    </div>
    </div>
 </div>