<div class="approach-content-block report-block item-9 d-flex flex-wrap align-items-center">
	<div class="approach-content-block-inn pos-rel w-100">
	<div class="approach-content">
	   <div class="report-img">
	      <img src="<?php the_sub_field('image');?>" alt="">
	   </div>
	   <?php if(have_rows('key_feature')):?>
	   <div class="key-feature key-feature-3 clr-light">
	      <ul class="clearfix">
	      	<?php while(have_rows('key_feature')): the_row();?>
	         <li><span class="connector"></span>
	         <?php the_sub_field('feature');?>
	         </li>
	     <?php endwhile;?>
	      </ul>
	   </div>
		<?php endif;?>
	</div>
	</div>
	</div>