<div class="approach-content-block has_bgvector item-8 d-flex flex-wrap align-items-center">
   <div class="approach-content-block-inn w-100">
   <div class="approach-content text-center">
      <div class="block-title text-center mb-3 d-none d-sm-block"><?php the_sub_field('heading');?></div>
      <div class="pwrap fsize-1 text-center clr-light mb-4">
         <p><?php the_sub_field('description');?></p>
      </div>
      <div class="block-img">
         <img src="<?php the_sub_field('image');?>" alt="">
      </div>
   </div>
   <img src="<?php the_sub_field('background_image');?>" alt="" class="bg-img">
</div>
</div>