<section class="section-six pt-100">
         <div class="section-top-content">
            <div class="container posrel">
               <h3 class="clr-white hashtag rotated"><?php the_sub_field('sub_heading');?></h3>
               <div class="section-heading text-uppercase clr-white fsize-md"><?php the_sub_field('heading');?>
               </div>
               <div class="img-holder text-center">
                  <img src="/wp-content/uploads/2021/06/doctor-vector.jpg" alt="">
               </div>   
            </div>
         </div>
         <div class="section-bottom-content pb-100">
            <div class="container">
               <div class="content-block">
                  <div class="para-holder clr-dark mb-3">
                     <p><?php the_sub_field('description');?></p>
                  </div>
                  <div class="list-holder clr-dark">
                     <ol>
                     	<?php if(have_rows('listing_content')): while(have_rows('listing_content')): the_row();?>
                        <li class="mb-2"><strong><?php the_sub_field('content');?></strong></li>
                    	<?php endwhile; endif;?>
                     </ol>
                  </div>
               </div>
            </div>
         </div>
      </section>