<section class="section-five pt-100 pb-100 posrel">
         <div class="container">
            <div class="row">
               <div class="col-lg-8 offset-lg-2">
                  <div class="content-block text-center">
                     <div class="section-heading text-uppercase clr-white heading-bg rotated fsize-md"><?php the_sub_field('heading');?></div>
                     <div class="para-holder mb-5">
                        <p><?php the_sub_field('description');?>
                        </p>
                     </div>
                     <div class="img-holder mb-5">
                        <img src="<?php the_sub_field('image');?>" width="400" alt="">
                     </div>
                     <div class="section-heading text-uppercase clr-dark rotated fsize-md"><?php the_sub_field('bottom_heading');?></div>
                  </div>
               </div>
            </div>
         </div>
      </section>