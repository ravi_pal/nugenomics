<section class="section-three pt-100 posrel">
         <div class="container">
            <div class="row">
               <div class="col-lg-7">
                  <div class="content-block">
                     <div class="section-heading text-uppercase clr-dark"><small class="font-primary"><?php the_sub_field('sub_heading');?></small><br> <?php the_sub_field('heading');?></div>
                     <div class="divider-line bgdark mb-5 mt-3"></div>
                     <div class="list-holder mb-4">
                        <ol>
                           <?php if(have_rows('listing_content')): while(have_rows('listing_content')): the_row();?>
                           <li class="mb-3"><?php the_sub_field('content');?></li>
                           <?php endwhile; endif;?>
                        </ol>
                     </div>
                     <div class="img-holder">
                        <img src="<?php the_sub_field('image');?>" width="300" alt="">
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="banner-img" style="background: url(<?php the_sub_field('backgroung_banner');?>) no-repeat right bottom;"></div>
      </section>