<div class="pinned-content-block item-1 d-flex flex-wrap align-items-center">
    <div class="pinned-content">
       <div class="block-title text-center mb-4"><?php the_sub_field('heading');?></div>
       <div class="pwrap text-center clr-light">
           <p><?php the_sub_field('description');?></p>
       </div>
    </div>
 </div>