<div class="pinned-content-block item-2 d-flex flex-wrap align-items-center">
    <div class="pinned-content">
        <div class="pwrap fsize-1 text-center clr-light mb-3"><p><?php the_sub_field('heading');?>Who are we exactly?</p></div>
       <div class="block-title fsize-md text-center mb-3"><?php the_sub_field('sub_heading');?>Four Nerds</div>
       <div class="block-img text-center">
         <img src="<?php the_sub_field('image');?>" width="400" alt="">
      </div>
    </div>
 </div>