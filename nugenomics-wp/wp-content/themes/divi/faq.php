<?php /*Template Name: Faq Page*/

get_header();?>

<section class="herosection content-section" style="background-color: <?php the_field('banner_background_color');?>;">
       <div id="herobanner">
          <div class="page-title"><?php the_field('banner_text');?></div>
          <div class="banner-img text-center anim-gif pl-3 pr-3">
            <?php $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); 
            $getsecondimage=$dynamic_featured_image->get_featu?>
             <img src="<?php echo $url; ?>" width="720" alt="">
          </div>
       </div>
    </section>
    <section class="content-section">
    <div class="container">
        <div class="block-title text-center mb-5"><?php the_field('page_title');?></div>
        <div id="accordion" class="faq-accordion">
            
          <?php $faq_posts = get_field('faq');
          if($faq_posts): $f=1;
            foreach($faq_posts AS $post):
              setup_postdata($post);?>

            <div class="faq-card">
              <div class="faq-card-header">
                <h5 class="faq-title <?php if($f != 1){?>collapsed<?php }?>" data-toggle="collapse" data-target="#collapse<?php echo $f;?>" aria-expanded="<?php if($f == 1){?>true<?php }else{?>false<?php }?>" aria-controls="collapse<?php echo $f;?>">
                  <img alt="" src="<?php bloginfo('template_directory'); ?>/images/icons/que-icon.svg" class="que-icon" />  <?php the_title();?> 
                </h5>
              </div>
          
              <div id="collapse<?php echo $f;?>" class="collapse <?php if($f==1){?>show<?php }?>" aria-labelledby="heading<?php echo $f;?>" data-parent="#accordion">
                <div class="faq-card-body clr-light">
                    <?php the_content();?>
                </div>
              </div>
            </div>
          <?php $f++; endforeach; endif;?>              
          </div>
    </div>
</section>
    
<?php get_footer();?>