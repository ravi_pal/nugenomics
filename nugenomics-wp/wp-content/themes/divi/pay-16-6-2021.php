<?php /*Template Name: Pay*/
session_start();
get_header();
if(isset($_POST['submitnow']))
{
$_SESSION['sess_primary_customer']=$_POST['primary_men'];
if($_SESSION['sess_primary_customer']!='')
{
$_SESSION['payemail']=$_POST['email'.$_SESSION['sess_primary_customer']]; 
$_SESSION['payphone']=$_POST['phone'.$_SESSION['sess_primary_customer']]; 
}
$finaldata=[];
for($k=0;$k<$_SESSION['qty'];$k++)
{
$finaldata[$k]['email']=$_POST['email'.$k];
$finaldata[$k]['phoneNumber']=$_POST['phone'.$k];
if($_POST['primary_men']==$k)
{
$finaldata[$k]['primary_customer']='true';
}
else
{
$finaldata[$k]['primary_customer']='false';	
}
}

$finaljson=json_encode($finaldata);
//die();
$wpdb->query( $wpdb->prepare(
    "INSERT INTO users (user_data, amount, make_primary, post_date) VALUES ( %s, %s, %s, %s )",
    array(
        $finaljson,
        $_SESSION['amount'],
        $_POST['primary_men'],
		wp_date('Y-m-d H:i:s'),
		)
));
$orderid = urlencode($wpdb->insert_id);
$_SESSION['sess_orderid']=$orderid;
?>
<script>
location.href='/pay/';
</script>
<?php 
}

?>

<?php  while ( have_posts() ) : the_post(); 
$url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); 

$passprice=$_SESSION['amount']*100;
?>

<link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/css/et_core.css" />
<div id="main-content">
          <div id="et-boc" class="et-boc">
             <div class="et-l et-l--post">
                <div class="et_builder_inner_content et_pb_gutters3">
				<div class="et_pb_section et_pb_section_0 et_pb_with_background et_section_regular" style="background: #f3f3f1 !important;">
                         <div class="et_pb_column et_pb_column_4_4 et_pb_column_0  et_pb_css_mix_blend_mode_passthrough et-last-child">
                            <div class="et_pb_module et_pb_text et_pb_text_0  et_pb_text_align_center et_pb_bg_layout_light">
                               <div class="et_pb_text_inner">
                                  <h1>Pay Now</h1>
                               </div>
                            </div>
                         </div>
                   </div>
                   
                   <div class="et_pb_section et_pb_section_3 et_pb_with_background et_section_regular" style="background-image: none !important;">
                      <div class="et_pb_row et_pb_row_5">
                         <div class="et_pb_column et_pb_column_1_2 et_pb_column_8  et_pb_css_mix_blend_mode_passthrough">
                            <div class="et_pb_module et_pb_text et_pb_text_7  et_pb_text_align_left et_pb_bg_layout_light">
                               <div class="et_pb_text_inner">
                                  <h3>Order Id</h3>
                                  <p>#<?php echo $_SESSION['sess_orderid'];?></p>
                               </div>
                            </div>
                            <div class="et_pb_module et_pb_text et_pb_text_8  et_pb_text_align_left et_pb_bg_layout_light">
                               <div class="et_pb_text_inner">
                                  <h3>Name</h3>
                                  <p><?php echo $_SESSION['productname'];?></p>
                               </div>
                            </div>
                            <div class="et_pb_module et_pb_text et_pb_text_8  et_pb_text_align_left et_pb_bg_layout_light">
                               <div class="et_pb_text_inner">
                                  <h3>Quantity</h3>
                                  <p><?php echo $_SESSION['qty'];?></p>
                               </div>
                            </div>
                            <div class="et_pb_module et_pb_text et_pb_text_9  et_pb_text_align_left et_pb_bg_layout_light">
                               <div class="et_pb_text_inner">
                                  <h3>Price</h3>
                                  <p>INR <?php echo $_SESSION['amount'];?></p>
<?php //echo $_SESSION['sess_primary_customer'];

?>

                               </div>
                            </div>
                            <div class="et_pb_button_module_wrapper et_pb_button_3_wrapper  et_pb_module ">
							<form action="/success/" method="POST">
	<script src="https://checkout.razorpay.com/v1/checkout.js"
	data-key="rzp_live_98OKAThumNxzmP"
	data-amount="<?php echo $passprice;?>"
	data-currency="<?php echo strtoupper($_SESSION['currency']);?>" 
	data-buttontext="Pay with Razorpay"
	data-name="Nugenomics"
	data-description="Nugenomics"
	data-image="<?php echo $_SESSION['img_src'];?>"
  data-prefill.email="<?php echo $_SESSION['payemail'];?>"
  data-prefill.contact="<?php echo $_SESSION['payphone'];?>"
	data-theme.color="#F37254"></script>
	<input type="hidden" custom="Hidden Element" name="hidden"></form>
                            </div>
                         </div>
                         <div class="et_pb_column et_pb_column_1_2 et_pb_column_9  et_pb_css_mix_blend_mode_passthrough et-last-child et_pb_column_empty"></div>
                      </div>
                   </div>
                </div>
             </div>
          </div>
 </div>

<?php endwhile; ?>
<?php get_footer();?>