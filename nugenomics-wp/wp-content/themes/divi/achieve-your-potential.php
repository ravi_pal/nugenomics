<?php /*Template Name: Achieve your Potential*/

get_header();?>

<section class="herosection">
         <div id="herobanner">
               <div class="banner-img">
                    <?php $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); 
                    $getsecondimage=$dynamic_featured_image->get_featured_images([$post->ID]);
                    $getimagesecondurl=$getsecondimage[0]['full'];?>
                  <img src="<?php echo $url;?>" class="d-none d-sm-block" width="100%" alt="">
                  <img src="<?php echo $getimagesecondurl;?>" class="d-block d-sm-none" width="100%" alt="">
            </div>
         </div>
      </section>
      <section class="content-section">
         <?php $contentWithHeading = get_field('content_with_heading');
         if($contentWithHeading):?>
         <div class="container">
            <div class="row">

                <div class="col-lg-8 offset-lg-2">
                    <div class="block-title text-center mb-30"><?php echo $contentWithHeading['heading'];?></div>
            <div class="pwrap fsize-1 text-center clr-light">
                <p><?php echo $contentWithHeading['description'];?></p>
            </div>
                </div>
            </div>
        </div>
    <?php endif;?>

    <?php $circle = get_field('circle_block_layout'); if($circle):?>
            <div class="revamp-cards-wrapper">
                <div class="container">
                <div class="block-title text-center"><?php echo $circle['heading'];?></div>
                <div class="revamp-cards clearfix">
                <img src="<?php bloginfo('template_directory'); ?>/images/bg-logo.png" class="bg-img" alt="">
                <div class="revamp-card card-1">
                    <div class="card-figure">
                        <img src="<?php echo $circle['circle_1_image'];?>" alt="">
                        <div class="card-figcaption block-title">
                            <?php echo $circle['circle_1_text'];?>
                        </div>
                    </div>
                    <div class="card-hover">
                        <div class="hover-meta">
                               <?php echo $circle['circle_1_description'];?> 
                       
                            </div>
                    </div>
                </div>
                <div class="revamp-card card-2">
                    <div class="card-figure">
                        <img src="<?php echo $circle['circle_2_image']?>" alt="">
                        <div class="card-figcaption block-title">
                            <?php echo $circle['circle_2_text']?>
                        </div>
                    </div>
                    <div class="card-hover">
                        <div class="hover-meta">
                            <?php echo $circle['circle_2_description'];?>
                        
                            </div>
                    </div>
                </div>
                <div class="revamp-card card-3">
                    <div class="card-figure">
                        <img src="<?php echo $circle['circle_3_image']?>" alt="">
                        <div class="card-figcaption block-title">
                            <?php echo $circle['circle_3_text'];?>
                        </div>
                    </div>
                    <div class="card-hover">
                        <div class="hover-meta">
                        <?php echo $circle['circle_3_description'];?>
                            </div>
                    </div>
                </div>
                <div class="revamp-card card-4">
                    <div class="card-figure">
                        <img src="<?php echo $circle['circle_4_image'];?>" alt="">
                        <div class="card-figcaption block-title">
                            <?php echo $circle['circle_4_text'];?>
                        </div>
                    </div>
                    <div class="card-hover">
                        <div class="hover-meta">
                            <?php echo $circle['circle_4_description'];?>
                        
                            </div>
                    </div>
                </div>
                <div class="revamp-card card-5">
                    <div class="card-figure">
                        <img src=" <?php echo $circle['circle_5_image'];?>" alt="">
                        <div class="card-figcaption block-title">
                             <?php echo $circle['circle_5_text'];?>
                        </div>
                    </div>
                    <div class="card-hover">
                        <div class="hover-meta">
                             <?php echo $circle['circle_5_description'];?>
                       
                            </div>
                    </div>
                </div>
                <div class="revamp-card card-6">
                    <div class="card-figure">
                        <img src="<?php echo $circle['circle_6_image'];?>" alt="">
                        <div class="card-figcaption block-title">
                            <?php echo $circle['circle_6_text'];?>
                        </div>
                    </div>
                    <div class="card-hover">
                        <div class="hover-meta">
                           <?php echo $circle['circle_6_description'];?>
                        </div>
                    </div>
                </div>
</div>

            </div>
         </div>
     <?php endif;?>
      </section>
<div class="modal fade custom-modal" id="frmModal" tabindex="-1" role="dialog" aria-labelledby="frmModalLabel" aria-hidden="true">
   <div class="modal-dialog" role="document">
     <div class="modal-content">
       <div class="modal-header">
         <h5 class="modal-title" id="frmModalLabel">Get in Touch</h5>
         <button type="button" class="close" data-dismiss="modal" aria-label="Close">
           <span aria-hidden="true">&times;</span>
         </button>
       </div>
       <div class="modal-body">
         <div class="row">
            <div class="col-lg-10 offset-lg-1">
               <div class="frm-holder mt-4">
<?php echo do_shortcode('[contact-form-7 id="218" title="Contact form 1"]');?>
               </div>
            </div>
         </div>
       </div>
     </div>
   </div>
 </div>
<?php get_footer();?>