<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'nugenomics' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '2f987e6c89662ccd37f5a2b1c37d88500518e17c20715d18' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

define('FS_METHOD', 'direct');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'laiXF(shgO`;nw:P8vujp-gn-R5oSQ3%AN=6~2+Dxv@>at@EF#1%;Z-!y2;H1j{i' );
define( 'SECURE_AUTH_KEY',  'Cgj5A1JxO.40d%C{_6mX2-A:av/(7WGp$3}s1B{l,:)H<4$hr+0n9V!D^B1hA<,z' );
define( 'LOGGED_IN_KEY',    '!%9hYB}rnsG.g@MLgs^Hg93+VY#9Q+>P,Sx-Jb.H`mPHmFXKKr-o5)D_ocGMt*ct' );
define( 'NONCE_KEY',        'P Pj;UvuGuQleR;yu3 Zc?@eO|$oeQq3{QIFv.?r4xcnpm%r>m!9xa&kxxrI+~[,' );
define( 'AUTH_SALT',        '0>P0WmmHmFZQi(BG3$rWTN3!we;l[r;1s(aO|?0_IR;Pa&6&8E#=HEJxg7on/:=}' );
define( 'SECURE_AUTH_SALT', 'K7G&wJOAnnta{y>ZJek=z7O;bp*q 6LH8y$.~fB7p,s_RHy^m]LB.YssL:9a>>wZ' );
define( 'LOGGED_IN_SALT',   'ZqG_0TQQQ;gz!cAY^)^(UH8uso3{-}Ldb;Eb;`(r;{!f3_vyr#sQJOz4/5$~;eV/' );
define( 'NONCE_SALT',       '?^+PNwCg[@PTb)hFa@R*cBst!}5%l%J,n<>fKCdRIP6,$Np^cx|UJ$i-$9fhi!cX' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
